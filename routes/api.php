<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BencanaController;
use App\Http\Controllers\KarhutlaBulanController;
use App\Http\Controllers\BanjirBulanController;

Route::prefix('json')->as('json.')->group(function () {
    Route::prefix('get')->group(function () {
        Route::post('bencana_edit', [BencanaController::class, 'json_edit'])->name('bencana_edit');
        Route::post('karhutlabulan_edit', [KarhutlaBulanController::class, 'json_edit'])->name('karhutlabulan_edit');
        Route::post('banjirbulan_edit', [BanjirBulanController::class, 'json_edit'])->name('banjirbulan_edit');
    });
});
