<?php

use App\Http\Controllers\BencanaController;
use App\Http\Controllers\BanjirController;
use App\Http\Controllers\KarhutlaBulanController;
use App\Http\Controllers\BanjirBulanController;
use App\Http\Controllers\PohonController;
use App\Http\Controllers\KarhutlaController;
use Illuminate\Support\Facades\Route;

Route::resource('bencana', BencanaController::class);
Route::resource('bencana/karhutla', KarhutlaController::class);
Route::resource('bencana/karhutla/data/k', KarhutlaBulanController::class);
Route::resource('bencana/banjir', BanjirController::class);
Route::resource('bencana/banjir/data/b', BanjirBulanController::class);
Route::resource('bencana/pohon', PohonController::class);
Route::get('/', [BencanaController::class, 'dashboard'])->name('dashboard');
Route::post('getKelurahan', [KarhutlaController::class, 'json_getKelurahan'])->name('getkelurahan');
Route::get('download/karhutla/bulan/image/{id}/{bulan}', [KarhutlaBulanController::class, 'makeimage'])->name('download.karhutla.bulan.image');
Route::get('download/karhutla/tahun/image/{id}', [KarhutlaController::class, 'makeimage'])->name('download.karhutla.tahun.image');
Route::get('bencana/karhutla/{id}/{bulan}', [KarhutlaBulanController::class, 'edit'])->name('karhutla.bulan.edit');

Route::get('download/banjir/bulan/image/{id}/{bulan}', [BanjirBulanController::class, 'makeimage'])->name('download.banjir.bulan.image');
Route::get('download/banjir/tahun/image/{id}', [BanjirController::class, 'makeimage'])->name('download.banjir.tahun.image');
Route::get('bencana/banjir/{id}/{bulan}', [BanjirBulanController::class, 'edit'])->name('banjir.bulan.edit');
