<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bencana;
use App\Models\BanjirBulan;
use App\Models\Banjir;
use Image;
use File;

class BanjirController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $bencana_id = $request->bencana_id;
        $bulan = $request->bulan;

        foreach ($bencana_id as $key => $k) {
            $input['bencana_id'] = $k;
            $input['bulan'] = $bulan[$key];
            Banjir::create($input);
        }

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Bulan berhasil dibuat...',
            ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $bencana = Bencana::findOrFail($id);
        $data_bulan = BanjirBulan::where('bencana_id', $id)->orderBy('district_id', 'DESC')->get();
        $banjir_bulan = Banjir::where('bencana_id', $id)->get();
        $bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $januari = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Januari')->get();
        $februari = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Februari')->get();
        $maret = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Maret')->get();
        $april = BanjirBulan::where('bencana_id', $id)->where('bulan', 'April')->get();
        $mei = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Mei')->get();
        $juni = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Juni')->get();
        $juli = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Juli')->get();
        $agustus = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Agustus')->get();
        $september = BanjirBulan::where('bencana_id', $id)->where('bulan', 'September')->get();
        $oktober = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Oktober')->get();
        $november = BanjirBulan::where('bencana_id', $id)->where('bulan', 'November')->get();
        $desember = BanjirBulan::where('bencana_id', $id)->where('bulan', 'Desember')->get();

        $kelurahan1 = BanjirBulan::where('bencana_id', $id)->where('district_id', 1)->sum('kelurahan_terdampak');
        $kelurahan2 = BanjirBulan::where('bencana_id', $id)->where('district_id', 2)->sum('kelurahan_terdampak');
        $kelurahan3 = BanjirBulan::where('bencana_id', $id)->where('district_id', 3)->sum('kelurahan_terdampak');
        $kelurahan4 = BanjirBulan::where('bencana_id', $id)->where('district_id', 4)->sum('kelurahan_terdampak');
        $kelurahan5 = BanjirBulan::where('bencana_id', $id)->where('district_id', 5)->sum('kelurahan_terdampak');
        $kelurahan6 = BanjirBulan::where('bencana_id', $id)->where('district_id', 6)->sum('kelurahan_terdampak');

        $penduduk1 = BanjirBulan::where('bencana_id', $id)->where('district_id', 1)->sum('penduduk_terdampak');
        $penduduk2 = BanjirBulan::where('bencana_id', $id)->where('district_id', 2)->sum('penduduk_terdampak');
        $penduduk3 = BanjirBulan::where('bencana_id', $id)->where('district_id', 3)->sum('penduduk_terdampak');
        $penduduk4 = BanjirBulan::where('bencana_id', $id)->where('district_id', 4)->sum('penduduk_terdampak');
        $penduduk5 = BanjirBulan::where('bencana_id', $id)->where('district_id', 5)->sum('penduduk_terdampak');
        $penduduk6 = BanjirBulan::where('bencana_id', $id)->where('district_id', 6)->sum('penduduk_terdampak');

        $rumah1 = BanjirBulan::where('bencana_id', $id)->where('district_id', 1)->sum('rumah_terendam');
        $rumah2 = BanjirBulan::where('bencana_id', $id)->where('district_id', 2)->sum('rumah_terendam');
        $rumah3 = BanjirBulan::where('bencana_id', $id)->where('district_id', 3)->sum('rumah_terendam');
        $rumah4 = BanjirBulan::where('bencana_id', $id)->where('district_id', 4)->sum('rumah_terendam');
        $rumah5 = BanjirBulan::where('bencana_id', $id)->where('district_id', 5)->sum('rumah_terendam');
        $rumah6 = BanjirBulan::where('bencana_id', $id)->where('district_id', 6)->sum('rumah_terendam');

        return view('page.bencana.banjir.index', compact('bencana', 'data_bulan', 'banjir_bulan', 'bulan', 'kelurahan1', 'kelurahan2', 'kelurahan3', 'kelurahan4', 'kelurahan5', 'kelurahan6', 'penduduk1', 'penduduk2', 'penduduk3', 'penduduk4', 'penduduk5', 'penduduk6', 'rumah1', 'rumah2', 'rumah3', 'rumah4', 'rumah5', 'rumah6', 'januari', 'februari', 'maret', 'april', 'mei', 'juni', 'juli', 'agustus', 'september', 'oktober', 'november', 'desember'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function makeimage($id)
    {
        $kelurahan_terdampak = BanjirBulan::where('bencana_id', $id)->sum('kelurahan_terdampak');
        $penduduk_terdampak = BanjirBulan::where('bencana_id', $id)->sum('penduduk_terdampak');
        $rumah_terendam = BanjirBulan::where('bencana_id', $id)->sum('rumah_terendam');
        $tahun = Bencana::where('id', $id)->first();
        $kolam_kelurahan = BanjirBulan::where('bencana_id', $id)->where('district_id', 1)->sum('kelurahan_terdampak');
        $kolam_penduduk = BanjirBulan::where('bencana_id', $id)->where('district_id', 1)->sum('penduduk_terdampak');
        $kolam_rumah = BanjirBulan::where('bencana_id', $id)->where('district_id', 1)->sum('rumah_terendam');
        $arsel_kelurahan = BanjirBulan::where('bencana_id', $id)->where('district_id', 2)->sum('kelurahan_terdampak');
        $arsel_penduduk = BanjirBulan::where('bencana_id', $id)->where('district_id', 2)->sum('penduduk_terdampak');
        $arsel_rumah = BanjirBulan::where('bencana_id', $id)->where('district_id', 2)->sum('rumah_terendam');
        $kumai_kelurahan = BanjirBulan::where('bencana_id', $id)->where('district_id', 3)->sum('kelurahan_terdampak');
        $kumai_penduduk = BanjirBulan::where('bencana_id', $id)->where('district_id', 3)->sum('penduduk_terdampak');
        $kumai_rumah = BanjirBulan::where('bencana_id', $id)->where('district_id', 3)->sum('rumah_terendam');
        $banteng_kelurahan = BanjirBulan::where('bencana_id', $id)->where('district_id', 4)->sum('kelurahan_terdampak');
        $banteng_penduduk = BanjirBulan::where('bencana_id', $id)->where('district_id', 4)->sum('penduduk_terdampak');
        $banteng_rumah = BanjirBulan::where('bencana_id', $id)->where('district_id', 4)->sum('rumah_terendam');
        $lada_kelurahan = BanjirBulan::where('bencana_id', $id)->where('district_id', 5)->sum('kelurahan_terdampak');
        $lada_penduduk = BanjirBulan::where('bencana_id', $id)->where('district_id', 5)->sum('penduduk_terdampak');
        $lada_rumah = BanjirBulan::where('bencana_id', $id)->where('district_id', 5)->sum('rumah_terendam');
        $aruta_kelurahan = BanjirBulan::where('bencana_id', $id)->where('district_id', 6)->sum('kelurahan_terdampak');
        $aruta_penduduk = BanjirBulan::where('bencana_id', $id)->where('district_id', 6)->sum('penduduk_terdampak');
        $aruta_rumah = BanjirBulan::where('bencana_id', $id)->where('district_id', 6)->sum('rumah_terendam');
        $img = Image::make(public_path('images/template/template_banjir.png'));

        #BULAN/TAHUN
        $waktu = 'Tahun ' . $tahun->tahun;
        $waktu_x = '142';
        $waktu_y = '101';
        $img->text(
            $waktu,
            $waktu_x,
            $waktu_y + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Bold.TTF'));
                $font->size(38);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #BULAN/TAHUN

        #TOTAL DESA/KELURAHAN TERDAMPAK
        $kelurahan = $kelurahan_terdampak;
        $total_x = '115';
        $total_y = '250';
        if (strlen($kelurahan) == 1) {
            $total_xh = $total_x + 18;
        } elseif (strlen($kelurahan) == 2) {
            $total_xh = $total_x + 32;
        } elseif (strlen($kelurahan) == 3) {
            $total_xh = $total_x + 45;
        } elseif (strlen($kelurahan) == 4) {
            $total_xh = $total_x + 58;
        } elseif (strlen($kelurahan) == 5) {
            $total_xh = $total_x + 70;
        } else {
            $total_xh = $total_x + 70;
        }
        $img->text(
            $kelurahan,
            $total_x,
            $total_y + 2,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Desa / Kelurahan',
            $total_xh,
            $total_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL DESA/KELURAHAN TERDAMPAK
        #TOTAL PENDUDUK TERDAMPAK
        $penduduk = $penduduk_terdampak;
        $total_x = '115';
        $total_y_penduduk = '282';
        if (strlen($penduduk) == 1) {
            $total_x_penduduk = $total_x + 18;
        } elseif (strlen($penduduk) == 2) {
            $total_x_penduduk = $total_x + 32;
        } elseif (strlen($penduduk) == 3) {
            $total_x_penduduk = $total_x + 45;
        } elseif (strlen($penduduk) == 4) {
            $total_x_penduduk = $total_x + 58;
        } elseif (strlen($penduduk) == 5) {
            $total_x_penduduk = $total_x + 70;
        } else {
            $total_x_penduduk = $total_x + 70;
        }
        $img->text(
            $penduduk,
            $total_x,
            $total_y_penduduk + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Penduduk Terdampak',
            $total_x_penduduk,
            $total_y_penduduk,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL PENDUDUK TERDAMPAK

        #TOTAL RUMAH TERENDAM
        $rumah = $rumah_terendam;
        $total_x = '115';
        $total_y_rumah = '311';
        if (strlen($rumah) == 1) {
            $total_x_rumah = $total_x + 18;
        } elseif (strlen($rumah) == 2) {
            $total_x_rumah = $total_x + 32;
        } elseif (strlen($rumah) == 3) {
            $total_x_rumah = $total_x + 45;
        } elseif (strlen($rumah) == 4) {
            $total_x_rumah = $total_x + 58;
        } elseif (strlen($rumah) == 5) {
            $total_x_rumah = $total_x + 70;
        } else {
            $total_x_rumah = $total_x + 70;
        }
        $img->text(
            $rumah,
            $total_x,
            $total_y_rumah + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Rumah Terendam',
            $total_x_rumah,
            $total_y_rumah,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        /* #TOTAL RUMAH TERENDAM */

        /* KOTAWARINGIN LAMA */
        #KELURAHAN
        $kolam_x = '493';
        $kolam_y = '206';
        $img->text(
            $kolam_kelurahan . ' Desa/Kelurahan',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $kolam_x = '493';
        $kolam_y = '240';
        $img->text(
            $kolam_penduduk . ' Penduduk',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $kolam_x = '493';
        $kolam_y = '273';
        $img->text(
            $kolam_rumah . ' Rumah',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* KOTAWARINGIN LAMA */

        /* ARUT SELATAN */
        #KELURAHAN
        $arsel_x = '496';
        $arsel_y = '648';
        $img->text(
            $arsel_kelurahan . ' Desa/Kelurahan',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $arsel_x = '496';
        $arsel_y = '682';
        $img->text(
            $arsel_penduduk . ' Penduduk',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $arsel_x = '496';
        $arsel_y = '715';
        $img->text(
            $arsel_rumah . ' Rumah',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* ARUT SELATAN */

        /* KUMAI */
        #KELURAHAN
        $kumai_x = '916';
        $kumai_y = '636';
        $img->text(
            $kumai_kelurahan . ' Desa/Kelurahan',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $kumai_x = '916';
        $kumai_y = '670';
        $img->text(
            $kumai_penduduk . ' Penduduk',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $kumai_x = '916';
        $kumai_y = '703';
        $img->text(
            $kumai_rumah . ' Rumah',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* KUMAI */

        /* PANGKALAN BANTENG */
        #KELURAHAN
        $banteng_x = '913';
        $banteng_y = '298';
        $img->text(
            $banteng_kelurahan . ' Desa/Kelurahan',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $banteng_x = '913';
        $banteng_y = '332';
        $img->text(
            $banteng_penduduk . ' Penduduk',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $banteng_x = '913';
        $banteng_y = '365';
        $img->text(
            $banteng_rumah . ' Rumah',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* PANGKALAN BANTENG */

        /* PANGKALAN LADA */
        #KELURAHAN
        $lada_x = '914';
        $lada_y = '468';
        $img->text(
            $lada_kelurahan . ' Desa/Kelurahan',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $lada_x = '914';
        $lada_y = '502';
        $img->text(
            $lada_penduduk . ' Penduduk',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $lada_x = '914';
        $lada_y = '535';
        $img->text(
            $lada_rumah . ' Rumah',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* PANGKALAN LADA */

        /* ARUT UTARA */
        #KELURAHAN
        $aruta_x = '882';
        $aruta_y = '123';
        $img->text(
            $aruta_kelurahan . ' Desa/Kelurahan',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $aruta_x = '882';
        $aruta_y = '157';
        $img->text(
            $aruta_penduduk . ' Penduduk',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $aruta_x = '882';
        $aruta_y = '190';
        $img->text(
            $aruta_rumah . ' Rumah',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* ARUT UTARA */


        // $img->save('images/hasil.png');

        $nama_file = 'Infografis Banjir Tahun ' . $tahun->tahun . '.png';
        File::isDirectory(public_path('images/download/')) or File::makeDirectory(public_path('images/download/'), 0777, true, true);
        $img->save(public_path('images/download/') . $nama_file);
        // return back();
        return response()->download(public_path('images/download/') . $nama_file);
        // return response()->download('images/hasil.png');
    }
}
