<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\District;
use App\Models\Bencana;
use App\Models\KarhutlaBulan;
use App\Models\Karhutla;
use Image;
use File;

class KarhutlaBulanController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $datas = new KarhutlaBulan;
        $datas->bencana_id = $request->bencana_id;
        $datas->district_id = $request->district_id;
        $datas->bulan = $request->bulan;
        $datas->jumlah_kejadian = $request->jumlah_kejadian;
        $datas->jumlah_hotspot = $request->jumlah_hotspot;
        $datas->luas_terbakar = $request->luas_terbakar;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Data telah diperbaharui...',
            ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id, $bulan)
    {
        $databulan = KarhutlaBulan::where('bencana_id', $id)->where('bulan', $bulan)->orderBy('district_id', 'ASC')->get();
        $getbulan = Karhutla::where('bencana_id', $id)->where('bulan', $bulan)->first();
        // $karhutla = Karhutla::where('bencana_id', $id)->get();
        $district = District::pluck('id', 'nama');
        $bencana = Bencana::findOrFail($id);
        $kolam = District::where('id', 1)->first();
        $arsel = District::where('id', 2)->first();
        $kumai = District::where('id', 3)->first();
        $banteng = District::where('id', 4)->first();
        $lada = District::where('id', 5)->first();
        $aruta = District::where('id', 6)->first();
        $ifkolam = KarhutlaBulan::where('district_id', 1)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifarsel = KarhutlaBulan::where('district_id', 2)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifkumai = KarhutlaBulan::where('district_id', 3)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifbanteng = KarhutlaBulan::where('district_id', 4)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $iflada = KarhutlaBulan::where('district_id', 5)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifaruta = KarhutlaBulan::where('district_id', 6)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        // $karhutla = Karhutla::where('bencana_id', $id)->orderBy('district_id', 'ASC')->get();
        $jenis = array('Karhutla', 'Banjir', 'Pohon Tumbang', 'Orang Hilang');
        // $bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        return view('page.bencana.karhutla.bulan.index', compact('databulan', 'jenis', 'getbulan', 'bencana', 'district', 'kolam', 'arsel', 'kumai', 'banteng', 'lada', 'aruta', 'ifkolam', 'ifarsel', 'ifkumai', 'ifbanteng', 'iflada', 'ifaruta'));
    }

    public function json_edit(request $request)
    {
        $data = KarhutlaBulan::where('bencana_id', $request->bencana_id)->where('district_id', $request->district_id)->where('bulan', $request->bulan)->first();
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $datas = KarhutlaBulan::findOrFail($id);
        $datas->bencana_id = $request->bencana_id;
        $datas->district_id = $request->district_id;
        $datas->bulan = $request->bulan;
        $datas->jumlah_kejadian = $request->jumlah_kejadian;
        $datas->jumlah_hotspot = $request->jumlah_hotspot;
        $datas->luas_terbakar = $request->luas_terbakar;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Data telah diperbaharui...',
            ]);
    }

    public function destroy($id)
    {
        //
    }

    public function makeimage($id, $bulan)
    {
        $jumlah_hotspot = KarhutlaBulan::where('bencana_id', $id)->where('bulan', $bulan)->sum('jumlah_hotspot');
        $jumlah_kejadian = KarhutlaBulan::where('bencana_id', $id)->where('bulan', $bulan)->sum('jumlah_kejadian');
        $jumlah_luas_terbakar = KarhutlaBulan::where('bencana_id', $id)->where('bulan', $bulan)->sum('luas_terbakar');
        $tahun = Bencana::where('id', $id)->first();
        $getbulan = KarhutlaBulan::where('bencana_id', $id)->where('bulan', $bulan)->first();
        $kolam = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 1)->where('bulan', $bulan)->first();
        $arsel = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 2)->where('bulan', $bulan)->first();
        $kumai = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 3)->where('bulan', $bulan)->first();
        $banteng = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 4)->where('bulan', $bulan)->first();
        $lada = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 5)->where('bulan', $bulan)->first();
        $aruta = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 6)->where('bulan', $bulan)->first();
        $img = Image::make(public_path('images/template/template_karhutla.png'));

        #BULAN/TAHUN
        $waktu = $getbulan->bulan . " " . $tahun->tahun;
        $waktu_x = '144';
        $waktu_y = '101';
        $img->text(
            $waktu,
            $waktu_x,
            $waktu_y + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Bold.TTF'));
                $font->size(38);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #BULAN/TAHUN

        #TOTAL HOTSPOT
        $hotspot = $jumlah_hotspot;
        $total_x = '140';
        $total_y = '254';
        if (strlen($hotspot) == 1) {
            $total_xh = $total_x + 14;
        } elseif (strlen($hotspot) == 2) {
            $total_xh = $total_x + 28;
        } elseif (strlen($hotspot) == 3) {
            $total_xh = $total_x + 42;
        } elseif (strlen($hotspot) == 4) {
            $total_xh = $total_x + 56;
        } elseif (strlen($hotspot) == 5) {
            $total_xh = $total_x + 70;
        } else {
            $total_xh = $total_x + 70;
        }
        $img->text(
            $hotspot,
            $total_x,
            $total_y + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Titik Hotspot',
            $total_xh,
            $total_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL HOTSPOT
        #TOTAL KEJADIAN
        $kejadian = $jumlah_kejadian;
        $total_x = '140';
        $total_y_kejadian = '282';
        if (strlen($kejadian) == 1) {
            $total_x_kejadian = $total_x + 18;
        } elseif (strlen($kejadian) == 2) {
            $total_x_kejadian = $total_x + 32;
        } elseif (strlen($kejadian) == 3) {
            $total_x_kejadian = $total_x + 45;
        } elseif (strlen($kejadian) == 4) {
            $total_x_kejadian = $total_x + 58;
        } elseif (strlen($kejadian) == 5) {
            $total_x_kejadian = $total_x + 70;
        } else {
            $total_x_kejadian = $total_x + 70;
        }
        $img->text(
            $kejadian,
            $total_x,
            $total_y_kejadian + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Kejadian Karhutla',
            $total_x_kejadian,
            $total_y_kejadian,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL KEJADIAN

        #TOTAL AREA TERBAKAR
        $luas_terbakar = $jumlah_luas_terbakar;
        $total_x = '140';
        $total_y_kejadian = '311';
        if (strlen($luas_terbakar) == 1) {
            $total_x_kejadian = $total_x + 18;
        } elseif (strlen($luas_terbakar) == 2) {
            $total_x_kejadian = $total_x + 32;
        } elseif (strlen($luas_terbakar) == 3) {
            $total_x_kejadian = $total_x + 45;
        } elseif (strlen($luas_terbakar) == 4) {
            $total_x_kejadian = $total_x + 58;
        } elseif (strlen($luas_terbakar) == 5) {
            $total_x_kejadian = $total_x + 70;
        } else {
            $total_x_kejadian = $total_x + 70;
        }
        $img->text(
            $luas_terbakar,
            $total_x,
            $total_y_kejadian + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Hektar Terbakar',
            $total_x_kejadian,
            $total_y_kejadian,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        /* #TOTAL AREA TERBAKAR */

        /* KOTAWARINGIN LAMA */
        #HOTSPOT
        $kolam_hotspot = $kolam->jumlah_hotspot;
        $kolam_x = '493';
        $kolam_y = '206';
        $img->text(
            $kolam_hotspot . ' Titik',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $kolam_kejadian = $kolam->jumlah_kejadian;
        $kolam_x = '493';
        $kolam_y = '240';
        $img->text(
            $kolam_kejadian . ' Kejadian',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $kolam_terbakar = $kolam->luas_terbakar;
        $kolam_x = '493';
        $kolam_y = '273';
        $img->text(
            $kolam_terbakar . ' Hektar',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* KOTAWARINGIN LAMA */

        /* ARUT SELATAN */
        #HOTSPOT
        $arsel_hotspot = $arsel->jumlah_hotspot;
        $arsel_x = '496';
        $arsel_y = '648';
        $img->text(
            $arsel_hotspot . ' Titik',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $arsel_kejadian = $arsel->jumlah_kejadian;
        $arsel_x = '496';
        $arsel_y = '682';
        $img->text(
            $arsel_kejadian . ' Kejadian',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $arsel_terbakar = $arsel->luas_terbakar;
        $arsel_x = '496';
        $arsel_y = '715';
        $img->text(
            $arsel_terbakar . ' Hektar',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* ARUT SELATAN */

        /* KUMAI */
        #HOTSPOT
        $kumai_hotspot = $kumai->jumlah_hotspot;
        $kumai_x = '916';
        $kumai_y = '636';
        $img->text(
            $kumai_hotspot . ' Titik',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $kumai_kejadian = $kumai->jumlah_kejadian;
        $kumai_x = '916';
        $kumai_y = '670';
        $img->text(
            $kumai_kejadian . ' Kejadian',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $kumai_terbakar = $kumai->luas_terbakar;
        $kumai_x = '916';
        $kumai_y = '703';
        $img->text(
            $kumai_terbakar . ' Hektar',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* KUMAI */

        /* PANGKALAN BANTENG */
        #HOTSPOT
        $banteng_hotspot = $banteng->jumlah_hotspot;
        $banteng_x = '913';
        $banteng_y = '298';
        $img->text(
            $banteng_hotspot . ' Titik',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $banteng_kejadian = $banteng->jumlah_kejadian;
        $banteng_x = '913';
        $banteng_y = '332';
        $img->text(
            $banteng_kejadian . ' Kejadian',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $banteng_terbakar = $banteng->luas_terbakar;
        $banteng_x = '913';
        $banteng_y = '365';
        $img->text(
            $banteng_terbakar . ' Hektar',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* PANGKALAN BANTENG */

        /* PANGKALAN LADA */
        #HOTSPOT
        $lada_hotspot = $lada->jumlah_hotspot;
        $lada_x = '939';
        $lada_y = '468';
        $img->text(
            $lada_hotspot . ' Titik',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $lada_kejadian = $lada->jumlah_kejadian;
        $lada_x = '939';
        $lada_y = '502';
        $img->text(
            $lada_kejadian . ' Kejadian',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $lada_terbakar = $lada->luas_terbakar;
        $lada_x = '939';
        $lada_y = '535';
        $img->text(
            $lada_terbakar . ' Hektar',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* PANGKALAN LADA */

        /* ARUT UTARA */
        #HOTSPOT
        $aruta_hotspot = $aruta->jumlah_hotspot;
        $aruta_x = '882';
        $aruta_y = '123';
        $img->text(
            $aruta_hotspot . ' Titik',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $aruta_kejadian = $aruta->jumlah_kejadian;
        $aruta_x = '882';
        $aruta_y = '157';
        $img->text(
            $aruta_kejadian . ' Kejadian',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $aruta_terbakar = $aruta->luas_terbakar;
        $aruta_x = '882';
        $aruta_y = '190';
        $img->text(
            $aruta_terbakar . ' Hektar',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* ARUT UTARA */

        $nama_file = 'Infografis Karhutla ' . $getbulan->bulan . " " . $tahun->tahun . '.png';
        File::isDirectory(public_path('images/download/')) or File::makeDirectory(public_path('images/download/'), 0777, true, true);
        $img->save(public_path('images/download/') . $nama_file);
        // return back();
        return response()->download(public_path('images/download/') . $nama_file);
    }
}
