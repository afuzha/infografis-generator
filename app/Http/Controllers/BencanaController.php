<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bencana;
use Illuminate\Support\Facades\Redirect;

class BencanaController extends Controller
{
    public function dashboard()
    {
        // return view('page.dashboard');
        return redirect()->route('bencana.index');
    }

    public function index()
    {
        $bencana = Bencana::orderBy('id', 'DESC')->get();
        $jenis = array('Karhutla', 'Banjir');
        return view('page.bencana.index', compact('bencana', 'jenis'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'jenis' => 'required',
            'tahun' => 'required',
        ];
        $customMessages = [
            'required' => ':Attribute tidak boleh kosong!',
        ];
        $this->validate($request, $rules, $customMessages);

        $datas = new Bencana;
        $datas->jenis = $request->jenis;
        $datas->tahun = $request->tahun;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Data Bencana telah ditambahkan...',
            ]);
    }

    public function json_edit(request $request)
    {
        $data = Bencana::where('id', $request->id)->first();
        return response()->json($data);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        $datas = Bencana::findOrFail($id);;
        $datas->jenis = $request->jenis;
        $datas->tahun = $request->tahun;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Data Bencana telah diperbaharui...',
            ]);
    }

    public function destroy($id)
    {
        $data = Bencana::findOrFail($id);
        // $data = Bencana::find($request->id)->delete();
        $data->delete();
        // Bencana::where('id', $id)->delete();

        return response()->json([
            'status' => 'test'
        ]);

        // return back()
        //     ->with('success', 'Berhasil!')
        //     ->with('isi', 'Data Bencana telah dihapus...');
    }
}
