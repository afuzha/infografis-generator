<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karhutla;
use App\Models\KarhutlaBulan;
use App\Models\Bencana;
use Intervention\Image\Facades\Image;
use File;

class KarhutlaController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $bencana_id = $request->bencana_id;
        $bulan = $request->bulan;

        foreach ($bencana_id as $key => $k) {
            $input['bencana_id'] = $k;
            $input['bulan'] = $bulan[$key];
            Karhutla::create($input);
        }

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Bulan berhasil dibuat...',
            ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $karhutla_bulan = Karhutla::where('bencana_id', $id)->get();
        $bybulan = Karhutla::where('bencana_id', $id)->first();
        $bencana = Bencana::findOrFail($id);
        $data_bulan = KarhutlaBulan::where('bencana_id', $id)->orderBy('district_id', 'DESC')->get();
        $bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $januari = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Januari')->get();
        $februari = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Februari')->get();
        $maret = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Maret')->get();
        $april = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'April')->get();
        $mei = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Mei')->get();
        $juni = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Juni')->get();
        $juli = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Juli')->get();
        $agustus = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Agustus')->get();
        $september = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'September')->get();
        $oktober = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Oktober')->get();
        $november = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'November')->get();
        $desember = KarhutlaBulan::where('bencana_id', $id)->where('bulan', 'Desember')->get();

        $kejadian1 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 1)->sum('jumlah_kejadian');
        $kejadian2 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 2)->sum('jumlah_kejadian');
        $kejadian3 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 3)->sum('jumlah_kejadian');
        $kejadian4 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 4)->sum('jumlah_kejadian');
        $kejadian5 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 5)->sum('jumlah_kejadian');
        $kejadian6 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 6)->sum('jumlah_kejadian');

        $hotspot1 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 1)->sum('jumlah_hotspot');
        $hotspot2 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 2)->sum('jumlah_hotspot');
        $hotspot3 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 3)->sum('jumlah_hotspot');
        $hotspot4 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 4)->sum('jumlah_hotspot');
        $hotspot5 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 5)->sum('jumlah_hotspot');
        $hotspot6 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 6)->sum('jumlah_hotspot');

        $luas_terbakar1 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 1)->sum('luas_terbakar');
        $luas_terbakar2 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 2)->sum('luas_terbakar');
        $luas_terbakar3 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 3)->sum('luas_terbakar');
        $luas_terbakar4 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 4)->sum('luas_terbakar');
        $luas_terbakar5 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 5)->sum('luas_terbakar');
        $luas_terbakar6 = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 6)->sum('luas_terbakar');

        return view('page.bencana.karhutla.index', compact('bencana', 'data_bulan', 'bulan', 'karhutla_bulan', 'kejadian1', 'kejadian2', 'kejadian3', 'kejadian4', 'kejadian5', 'kejadian6', 'hotspot1', 'hotspot2', 'hotspot3', 'hotspot4', 'hotspot5', 'hotspot6', 'luas_terbakar1', 'luas_terbakar2', 'luas_terbakar3', 'luas_terbakar4', 'luas_terbakar5', 'luas_terbakar6', 'januari', 'februari', 'maret', 'april', 'mei', 'juni', 'juli', 'agustus', 'september', 'oktober', 'november', 'desember'));
    }

    public function update(Request $request, $id)
    {
        $datas = Karhutla::findOrFail($id);
        $datas->bencana_id = $request->bencana_id;
        $datas->district_id = $request->district_id;
        $datas->bulan = $request->bulan;
        $datas->tahun = $request->tahun;
        $datas->jumlah_kejadian = $request->jumlah_kejadian;
        $datas->jumlah_hotspot = $request->jumlah_hotspot;
        $datas->luas_terbakar = $request->luas_terbakar;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Data telah diperbaharui...',
            ]);
    }

    public function destroy($id)
    {
        //
    }

    public function makeimage($id)
    {
        $jumlah_hotspot = KarhutlaBulan::where('bencana_id', $id)->sum('jumlah_hotspot');
        $jumlah_kejadian = KarhutlaBulan::where('bencana_id', $id)->sum('jumlah_kejadian');
        $jumlah_luas_terbakar = KarhutlaBulan::where('bencana_id', $id)->sum('luas_terbakar');
        $tahun = Bencana::where('id', $id)->first();
        $kolam_hotspot = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 1)->sum('jumlah_hotspot');
        $kolam_kejadian = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 1)->sum('jumlah_kejadian');
        $kolam_terbakar = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 1)->sum('luas_terbakar');
        $arsel_hotspot = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 2)->sum('jumlah_hotspot');
        $arsel_kejadian = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 2)->sum('jumlah_kejadian');
        $arsel_terbakar = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 2)->sum('luas_terbakar');
        $kumai_hotspot = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 3)->sum('jumlah_hotspot');
        $kumai_kejadian = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 3)->sum('jumlah_kejadian');
        $kumai_terbakar = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 3)->sum('luas_terbakar');
        $banteng_hotspot = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 4)->sum('jumlah_hotspot');
        $banteng_kejadian = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 4)->sum('jumlah_kejadian');
        $banteng_terbakar = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 4)->sum('luas_terbakar');
        $lada_hotspot = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 5)->sum('jumlah_hotspot');
        $lada_kejadian = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 5)->sum('jumlah_kejadian');
        $lada_terbakar = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 5)->sum('luas_terbakar');
        $aruta_hotspot = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 6)->sum('jumlah_hotspot');
        $aruta_kejadian = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 6)->sum('jumlah_kejadian');
        $aruta_terbakar = KarhutlaBulan::where('bencana_id', $id)->where('district_id', 6)->sum('luas_terbakar');
        $img = Image::make(public_path('images/template/template_karhutla.png'));

        #BULAN/TAHUN
        $waktu = 'Tahun ' . $tahun->tahun;
        $waktu_x = '142';
        $waktu_y = '101';
        $img->text(
            $waktu,
            $waktu_x,
            $waktu_y + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Bold.TTF'));
                $font->size(38);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #BULAN/TAHUN

        #TOTAL HOTSPOT
        $hotspot = $jumlah_hotspot;
        $total_x = '140';
        $total_y = '254';
        if (strlen($hotspot) == 1) {
            $total_xh = $total_x + 14;
        } elseif (strlen($hotspot) == 2) {
            $total_xh = $total_x + 28;
        } elseif (strlen($hotspot) == 3) {
            $total_xh = $total_x + 42;
        } elseif (strlen($hotspot) == 4) {
            $total_xh = $total_x + 56;
        } elseif (strlen($hotspot) == 5) {
            $total_xh = $total_x + 70;
        } else {
            $total_xh = $total_x + 70;
        }
        $img->text(
            $hotspot,
            $total_x,
            $total_y + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Titik Hotspot',
            $total_xh,
            $total_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL HOTSPOT
        #TOTAL KEJADIAN
        $kejadian = $jumlah_kejadian;
        $total_x = '140';
        $total_y_kejadian = '282';
        if (strlen($kejadian) == 1) {
            $total_x_kejadian = $total_x + 18;
        } elseif (strlen($kejadian) == 2) {
            $total_x_kejadian = $total_x + 32;
        } elseif (strlen($kejadian) == 3) {
            $total_x_kejadian = $total_x + 45;
        } elseif (strlen($kejadian) == 4) {
            $total_x_kejadian = $total_x + 58;
        } elseif (strlen($kejadian) == 5) {
            $total_x_kejadian = $total_x + 70;
        } else {
            $total_x_kejadian = $total_x + 70;
        }
        $img->text(
            $kejadian,
            $total_x,
            $total_y_kejadian + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Kejadian Karhutla',
            $total_x_kejadian,
            $total_y_kejadian,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL KEJADIAN

        #TOTAL AREA TERBAKAR
        $luas_terbakar = $jumlah_luas_terbakar;
        $total_x = '140';
        $total_y_kejadian = '311';
        if (strlen($luas_terbakar) == 1) {
            $total_x_kejadian = $total_x + 18;
        } elseif (strlen($luas_terbakar) == 2) {
            $total_x_kejadian = $total_x + 32;
        } elseif (strlen($luas_terbakar) == 3) {
            $total_x_kejadian = $total_x + 45;
        } elseif (strlen($luas_terbakar) == 4) {
            $total_x_kejadian = $total_x + 58;
        } elseif (strlen($luas_terbakar) == 5) {
            $total_x_kejadian = $total_x + 70;
        } else {
            $total_x_kejadian = $total_x + 70;
        }
        $img->text(
            $luas_terbakar,
            $total_x,
            $total_y_kejadian + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Hektar Terbakar',
            $total_x_kejadian,
            $total_y_kejadian,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        /* #TOTAL AREA TERBAKAR */

        /* KOTAWARINGIN LAMA */
        #HOTSPOT
        $kolam_x = '493';
        $kolam_y = '206';
        $img->text(
            $kolam_hotspot . ' Titik',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $kolam_x = '493';
        $kolam_y = '240';
        $img->text(
            $kolam_kejadian . ' Kejadian',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $kolam_x = '493';
        $kolam_y = '273';
        $img->text(
            $kolam_terbakar . ' Hektar',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* KOTAWARINGIN LAMA */

        /* ARUT SELATAN */
        #HOTSPOT
        $arsel_x = '496';
        $arsel_y = '648';
        $img->text(
            $arsel_hotspot . ' Titik',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $arsel_x = '496';
        $arsel_y = '682';
        $img->text(
            $arsel_kejadian . ' Kejadian',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $arsel_x = '496';
        $arsel_y = '715';
        $img->text(
            $arsel_terbakar . ' Hektar',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* ARUT SELATAN */

        /* KUMAI */
        #HOTSPOT
        $kumai_x = '916';
        $kumai_y = '636';
        $img->text(
            $kumai_hotspot . ' Titik',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $kumai_x = '916';
        $kumai_y = '670';
        $img->text(
            $kumai_kejadian . ' Kejadian',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $kumai_x = '916';
        $kumai_y = '703';
        $img->text(
            $kumai_terbakar . ' Hektar',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* KUMAI */

        /* PANGKALAN BANTENG */
        #HOTSPOT
        $banteng_x = '913';
        $banteng_y = '298';
        $img->text(
            $banteng_hotspot . ' Titik',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $banteng_x = '913';
        $banteng_y = '332';
        $img->text(
            $banteng_kejadian . ' Kejadian',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $banteng_x = '913';
        $banteng_y = '365';
        $img->text(
            $banteng_terbakar . ' Hektar',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* PANGKALAN BANTENG */

        /* PANGKALAN LADA */
        #HOTSPOT
        $lada_x = '939';
        $lada_y = '468';
        $img->text(
            $lada_hotspot . ' Titik',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $lada_x = '939';
        $lada_y = '502';
        $img->text(
            $lada_kejadian . ' Kejadian',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $lada_x = '939';
        $lada_y = '535';
        $img->text(
            $lada_terbakar . ' Hektar',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* PANGKALAN LADA */

        /* ARUT UTARA */
        #HOTSPOT
        $aruta_x = '882';
        $aruta_y = '123';
        $img->text(
            $aruta_hotspot . ' Titik',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #HOTSPOT
        #KEJADIAN
        $aruta_x = '882';
        $aruta_y = '157';
        $img->text(
            $aruta_kejadian . ' Kejadian',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KEJADIAN
        #LUAS TERBAKAR
        $aruta_x = '882';
        $aruta_y = '190';
        $img->text(
            $aruta_terbakar . ' Hektar',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #LUAS TERBAKAR
        /* ARUT UTARA */

        $nama_file = 'Infografis Karhutla ' . $tahun->tahun . '.png';
        File::isDirectory(public_path('images/download/')) or File::makeDirectory(public_path('images/download/'), 0777, true, true);
        $img->save(public_path('images/download/') . $nama_file);
        // return back();
        return response()->download(public_path('images/download/') . $nama_file);
    }
}
