<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BanjirBulan;
use App\Models\Banjir;
use App\Models\District;
use App\Models\Bencana;
use Intervention\Image\Facades\Image;
use File;

class BanjirBulanController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $datas = new BanjirBulan;
        $datas->bencana_id = $request->bencana_id;
        $datas->district_id = $request->district_id;
        $datas->bulan = $request->bulan;
        $datas->kelurahan_terdampak = $request->kelurahan_terdampak;
        $datas->penduduk_terdampak = $request->penduduk_terdampak;
        $datas->rumah_terendam = $request->rumah_terendam;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Data telah disimpan...',
            ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id, $bulan)
    {
        $databulan = BanjirBulan::where('bencana_id', $id)->where('bulan', $bulan)->orderBy('district_id', 'ASC')->get();
        $getbulan = Banjir::where('bencana_id', $id)->where('bulan', $bulan)->first();
        // $karhutla = Karhutla::where('bencana_id', $id)->get();
        $district = District::pluck('id', 'nama');
        $bencana = Bencana::findOrFail($id);
        $kolam = District::where('id', 1)->first();
        $arsel = District::where('id', 2)->first();
        $kumai = District::where('id', 3)->first();
        $banteng = District::where('id', 4)->first();
        $lada = District::where('id', 5)->first();
        $aruta = District::where('id', 6)->first();
        $ifkolam = BanjirBulan::where('district_id', 1)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifarsel = BanjirBulan::where('district_id', 2)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifkumai = BanjirBulan::where('district_id', 3)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifbanteng = BanjirBulan::where('district_id', 4)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $iflada = BanjirBulan::where('district_id', 5)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        $ifaruta = BanjirBulan::where('district_id', 6)->where('bencana_id', $id)->where('bulan', $bulan)->get();
        // $karhutla = Karhutla::where('bencana_id', $id)->orderBy('district_id', 'ASC')->get();
        $jenis = array('Karhutla', 'Banjir', 'Pohon Tumbang', 'Orang Hilang');
        // $bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        return view('page.bencana.banjir.bulan.index', compact('databulan', 'jenis', 'getbulan', 'bencana', 'district', 'kolam', 'arsel', 'kumai', 'banteng', 'lada', 'aruta', 'ifkolam', 'ifarsel', 'ifkumai', 'ifbanteng', 'iflada', 'ifaruta'));
    }

    public function json_edit(request $request)
    {
        $data = BanjirBulan::where('bencana_id', $request->bencana_id)->where('district_id', $request->district_id)->where('bulan', $request->bulan)->first();
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $datas = BanjirBulan::findOrFail($id);
        $datas->bencana_id = $request->bencana_id;
        $datas->district_id = $request->district_id;
        $datas->bulan = $request->bulan;
        $datas->kelurahan_terdampak = $request->kelurahan_terdampak;
        $datas->penduduk_terdampak = $request->penduduk_terdampak;
        $datas->rumah_terendam = $request->rumah_terendam;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Data telah diperbaharui...',
            ]);
    }

    public function destroy($id)
    {
        //
    }

    public function makeimage($id, $bulan)
    {
        $kelurahan_terdampak = BanjirBulan::where('bencana_id', $id)->where('bulan', $bulan)->sum('kelurahan_terdampak');
        $penduduk_terdampak = BanjirBulan::where('bencana_id', $id)->where('bulan', $bulan)->sum('penduduk_terdampak');
        $rumah_terendam = BanjirBulan::where('bencana_id', $id)->where('bulan', $bulan)->sum('rumah_terendam');
        $tahun = Bencana::where('id', $id)->first();
        $getbulan = BanjirBulan::where('bencana_id', $id)->where('bulan', $bulan)->first();
        $kolam = BanjirBulan::where('bencana_id', $id)->where('district_id', 1)->where('bulan', $bulan)->first();
        $arsel = BanjirBulan::where('bencana_id', $id)->where('district_id', 2)->where('bulan', $bulan)->first();
        $kumai = BanjirBulan::where('bencana_id', $id)->where('district_id', 3)->where('bulan', $bulan)->first();
        $banteng = BanjirBulan::where('bencana_id', $id)->where('district_id', 4)->where('bulan', $bulan)->first();
        $lada = BanjirBulan::where('bencana_id', $id)->where('district_id', 5)->where('bulan', $bulan)->first();
        $aruta = BanjirBulan::where('bencana_id', $id)->where('district_id', 6)->where('bulan', $bulan)->first();
        $img = Image::make(public_path('images/template/template_banjir.png'));

        #BULAN/TAHUN
        $waktu = $getbulan->bulan . " " . $tahun->tahun;
        $waktu_x = '144';
        $waktu_y = '101';
        $img->text(
            $waktu,
            $waktu_x,
            $waktu_y + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Bold.TTF'));
                $font->size(38);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #BULAN/TAHUN

        #TOTAL DESA/KELURAHAN TERDAMPAK
        $kelurahan = $kelurahan_terdampak;
        $total_x = '115';
        $total_y = '250';
        if (strlen($kelurahan) == 1) {
            $total_xh = $total_x + 18;
        } elseif (strlen($kelurahan) == 2) {
            $total_xh = $total_x + 32;
        } elseif (strlen($kelurahan) == 3) {
            $total_xh = $total_x + 45;
        } elseif (strlen($kelurahan) == 4) {
            $total_xh = $total_x + 58;
        } elseif (strlen($kelurahan) == 5) {
            $total_xh = $total_x + 70;
        } else {
            $total_xh = $total_x + 70;
        }
        $img->text(
            $kelurahan,
            $total_x,
            $total_y + 2,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Desa/Kelurahan',
            $total_xh,
            $total_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL DESA/KELURAHAN TERDAMPAK
        #TOTAL PENDUDUK TERDAMPAK
        $penduduk = $penduduk_terdampak;
        $total_x = '115';
        $total_y_penduduk = '282';
        if (strlen($penduduk) == 1) {
            $total_x_penduduk = $total_x + 18;
        } elseif (strlen($penduduk) == 2) {
            $total_x_penduduk = $total_x + 32;
        } elseif (strlen($penduduk) == 3) {
            $total_x_penduduk = $total_x + 45;
        } elseif (strlen($penduduk) == 4) {
            $total_x_penduduk = $total_x + 58;
        } elseif (strlen($penduduk) == 5) {
            $total_x_penduduk = $total_x + 70;
        } else {
            $total_x_penduduk = $total_x + 70;
        }
        $img->text(
            $penduduk,
            $total_x,
            $total_y_penduduk + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Penduduk Terdampak',
            $total_x_penduduk,
            $total_y_penduduk,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        #TOTAL PENDUDUK TERDAMPAK

        #TOTAL RUMAH TERENDAM
        $rumah = $rumah_terendam;
        $total_x = '115';
        $total_y_rumah = '311';
        if (strlen($rumah) == 1) {
            $total_x_rumah = $total_x + 18;
        } elseif (strlen($rumah) == 2) {
            $total_x_rumah = $total_x + 32;
        } elseif (strlen($rumah) == 3) {
            $total_x_rumah = $total_x + 45;
        } elseif (strlen($rumah) == 4) {
            $total_x_rumah = $total_x + 58;
        } elseif (strlen($rumah) == 5) {
            $total_x_rumah = $total_x + 70;
        } else {
            $total_x_rumah = $total_x + 70;
        }
        $img->text(
            $rumah,
            $total_x,
            $total_y_rumah + 1,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        $img->text(
            'Rumah Terendam',
            $total_x_rumah,
            $total_y_rumah,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(25);
                $font->color('#ffffff');
                $font->align('left');
                $font->valign('top');
            }
        );
        /* #TOTAL RUMAH TERENDAM */

        /* KOTAWARINGIN LAMA */
        #KELURAHAN
        $kolam_kelurahan = $kolam->kelurahan_terdampak;
        $kolam_x = '493';
        $kolam_y = '206';
        $img->text(
            $kolam_kelurahan . ' Desa/Kelurahan',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $kolam_penduduk = $kolam->penduduk_terdampak;
        $kolam_x = '493';
        $kolam_y = '240';
        $img->text(
            $kolam_penduduk . ' Penduduk',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $kolam_rumah = $kolam->rumah_terendam;
        $kolam_x = '493';
        $kolam_y = '273';
        $img->text(
            $kolam_rumah . ' Rumah',
            $kolam_x,
            $kolam_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* KOTAWARINGIN LAMA */

        /* ARUT SELATAN */
        #KELURAHAN
        $arsel_kelurahan = $arsel->kelurahan_terdampak;
        $arsel_x = '496';
        $arsel_y = '648';
        $img->text(
            $arsel_kelurahan . ' Desa/Kelurahan',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $arsel_penduduk = $arsel->penduduk_terdampak;
        $arsel_x = '496';
        $arsel_y = '682';
        $img->text(
            $arsel_penduduk . ' Penduduk',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $arsel_rumah = $arsel->rumah_terendam;
        $arsel_x = '496';
        $arsel_y = '715';
        $img->text(
            $arsel_rumah . ' Rumah',
            $arsel_x,
            $arsel_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* ARUT SELATAN */

        /* KUMAI */
        #KELURAHAN
        $kumai_kelurahan = $kumai->kelurahan_terdampak;
        $kumai_x = '916';
        $kumai_y = '636';
        $img->text(
            $kumai_kelurahan . ' Desa/Kelurahan',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $kumai_penduduk = $kumai->penduduk_terdampak;
        $kumai_x = '916';
        $kumai_y = '670';
        $img->text(
            $kumai_penduduk . ' Penduduk',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $kumai_rumah = $kumai->rumah_terendam;
        $kumai_x = '916';
        $kumai_y = '703';
        $img->text(
            $kumai_rumah . ' Rumah',
            $kumai_x,
            $kumai_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* KUMAI */

        /* PANGKALAN BANTENG */
        #KELURAHAN
        $banteng_kelurahan = $banteng->kelurahan_terdampak;
        $banteng_x = '913';
        $banteng_y = '298';
        $img->text(
            $banteng_kelurahan . ' Desa/Kelurahan',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $banteng_penduduk = $banteng->penduduk_terdampak;
        $banteng_x = '913';
        $banteng_y = '332';
        $img->text(
            $banteng_penduduk . ' Penduduk',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $banteng_rumah = $banteng->rumah_terendam;
        $banteng_x = '913';
        $banteng_y = '365';
        $img->text(
            $banteng_rumah . ' Rumah',
            $banteng_x,
            $banteng_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* PANGKALAN BANTENG */

        /* PANGKALAN LADA */
        #KELURAHAN
        $lada_kelurahan = $lada->kelurahan_terdampak;
        $lada_x = '914';
        $lada_y = '468';
        $img->text(
            $lada_kelurahan . ' Desa/Kelurahan',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $lada_penduduk = $lada->penduduk_terdampak;
        $lada_x = '914';
        $lada_y = '502';
        $img->text(
            $lada_penduduk . ' Penduduk',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $lada_rumah = $lada->rumah_terendam;
        $lada_x = '914';
        $lada_y = '535';
        $img->text(
            $lada_rumah . ' Rumah',
            $lada_x,
            $lada_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* PANGKALAN LADA */

        /* ARUT UTARA */
        #KELURAHAN
        $aruta_kelurahan = $aruta->kelurahan_terdampak;
        $aruta_x = '882';
        $aruta_y = '123';
        $img->text(
            $aruta_kelurahan . ' Desa/Kelurahan',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #KELURAHAN
        #PENDUDUK
        $aruta_penduduk = $aruta->penduduk_terdampak;
        $aruta_x = '882';
        $aruta_y = '157';
        $img->text(
            $aruta_penduduk . ' Penduduk',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #PENDUDUK
        #RUMAH
        $aruta_rumah = $aruta->rumah_terendam;
        $aruta_x = '882';
        $aruta_y = '190';
        $img->text(
            $aruta_rumah . ' Rumah',
            $aruta_x,
            $aruta_y,
            function ($font) {
                $font->file(public_path('font/Calibri-Regular.ttf'));
                $font->size(18);
                $font->color('#f6ff00');
                $font->align('left');
                $font->valign('top');
            }
        );
        #RUMAH
        /* ARUT UTARA */


        // $img->save('images/hasil.png');

        $nama_file = 'Infografis Banjir ' . $getbulan->bulan . " " . $tahun->tahun . '.png';
        File::isDirectory(public_path('images/download/')) or File::makeDirectory(public_path('images/download/'), 0777, true, true);
        $img->save(public_path('images/download/') . $nama_file);
        // return back();
        return response()->download(public_path('images/download/') . $nama_file);
        // return response()->download('images/hasil.png');
    }
}
