<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BanjirBulan extends Model
{
    use HasFactory;
    protected $table = 'banjir_bulan';
    protected $fillable = [
        'bencana_id',
        'kelurahan_terdampak',
        'penduduk_terdampak',
        'rumah_terendam',
        'bulan',
        'district_id',
    ];

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }
}
