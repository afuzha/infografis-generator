<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KarhutlaBulan extends Model
{
    use HasFactory;
    protected $table = 'karhutla_bulan';
    protected $fillable = [
        'bencana_id',
        'jumlah_kejadian',
        'jumlah_hotspot',
        'luas_terbakar',
        'district_id',
        'bulan',
    ];
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }
}
