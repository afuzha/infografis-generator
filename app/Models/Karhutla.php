<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karhutla extends Model
{
    use HasFactory;
    protected $table = 'karhutla';
    protected $fillable = [
        'bencana_id',
        'bulan',
    ];
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }
}
