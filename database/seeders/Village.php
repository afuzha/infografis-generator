<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class Village extends Seeder
{
    public function run()
    {
        DB::table('village')->insert([
            #KOLAM#
            [
                'id' => '1',
                'district_id' => '1',
                'nama' => 'BABUAL BABOTI',
            ],
            [
                'id' => '2',
                'district_id' => '1',
                'nama' => 'TEMPAYUNG',
            ],
            [
                'id' => '3',
                'district_id' => '1',
                'nama' => 'SAKABULIN',
            ],
            [
                'id' => '4',
                'district_id' => '1',
                'nama' => 'KINJIL',
            ],
            [
                'id' => '5',
                'district_id' => '1',
                'nama' => 'KOTA WARINGIN HILIR',
            ],
            [
                'id' => '6',
                'district_id' => '1',
                'nama' => 'RIAM DURIAN',
            ],
            [
                'id' => '7',
                'district_id' => '1',
                'nama' => 'DAWAK',
            ],
            [
                'id' => '8',
                'district_id' => '1',
                'nama' => 'KOTAWARINGIN HULU',
            ],
            [
                'id' => '9',
                'district_id' => '1',
                'nama' => 'LALANG',
            ],
            [
                'id' => '10',
                'district_id' => '1',
                'nama' => 'KONDANG',
            ],
            [
                'id' => '11',
                'district_id' => '1',
                'nama' => 'SUKAMULYA',
            ],
            [
                'id' => '12',
                'district_id' => '1',
                'nama' => 'SUKAJAYA',
            ],
            [
                'id' => '13',
                'district_id' => '1',
                'nama' => 'SUKA MAKMUR',
            ],
            [
                'id' => '14',
                'district_id' => '1',
                'nama' => 'IPUH BANGUN JAYA',
            ],
            [
                'id' => '15',
                'district_id' => '1',
                'nama' => 'SUMBER MUKTI',
            ],
            [
                'id' => '16',
                'district_id' => '1',
                'nama' => 'PALIH BARU',
            ],
            #KOLAM#

            #ARSEL#
            [
                'id' => '17',
                'district_id' => '2',
                'nama' => 'TANJUNG PUTRI',
            ],
            [
                'id' => '18',
                'district_id' => '2',
                'nama' => 'KUMPAI BATU BAWAH',
            ],
            [
                'id' => '19',
                'district_id' => '2',
                'nama' => 'KUMPAI BATU ATAS',
            ],
            [
                'id' => '20',
                'district_id' => '2',
                'nama' => 'PASIR PANJANG',
            ],
            [
                'id' => '21',
                'district_id' => '2',
                'nama' => 'MENDAWAI',
            ],
            [
                'id' => '22',
                'district_id' => '2',
                'nama' => 'MENDAWAI SEBERANG',
            ],
            [
                'id' => '23',
                'district_id' => '2',
                'nama' => 'RAJA',
            ],
            [
                'id' => '24',
                'district_id' => '2',
                'nama' => 'SIDOREJO',
            ],
            [
                'id' => '25',
                'district_id' => '2',
                'nama' => 'MADUREJO',
            ],
            [
                'id' => '26',
                'district_id' => '2',
                'nama' => 'BARU',
            ],
            [
                'id' => '27',
                'district_id' => '2',
                'nama' => 'RAJA SEBERANG',
            ],
            [
                'id' => '28',
                'district_id' => '2',
                'nama' => 'RANGDA',
            ],
            [
                'id' => '29',
                'district_id' => '2',
                'nama' => 'KENAMBUI',
            ],
            [
                'id' => '30',
                'district_id' => '2',
                'nama' => 'UMPANG',
            ],
            [
                'id' => '31',
                'district_id' => '2',
                'nama' => 'NATAI RAYA',
            ],
            [
                'id' => '32',
                'district_id' => '2',
                'nama' => 'MEDANGSARI',
            ],
            [
                'id' => '33',
                'district_id' => '2',
                'nama' => 'NATAI BARU',
            ],
            [
                'id' => '34',
                'district_id' => '2',
                'nama' => 'TANJUNG TERANTANG',
            ],
            #ARSEL#

            #KUMAI#
            [
                'id' => '35',
                'district_id' => '3',
                'nama' => 'SUNGAI CABANG',
            ],
            [
                'id' => '36',
                'district_id' => '3',
                'nama' => 'TELUK PULAI',
            ],
            [
                'id' => '37',
                'district_id' => '3',
                'nama' => 'SUNGAI SEKONYER',
            ],
            [
                'id' => '38',
                'district_id' => '3',
                'nama' => 'SUNGAI BAKAU',
            ],
            [
                'id' => '39',
                'district_id' => '3',
                'nama' => 'TELUK BOGAM',
            ],
            [
                'id' => '40',
                'district_id' => '3',
                'nama' => 'KERAYA',
            ],
            [
                'id' => '41',
                'district_id' => '3',
                'nama' => 'SEBUAI',
            ],
            [
                'id' => '42',
                'district_id' => '3',
                'nama' => 'SUNGAI KAPITAN',
            ],
            [
                'id' => '43',
                'district_id' => '3',
                'nama' => 'KUMAI HILIR',
            ],
            [
                'id' => '44',
                'district_id' => '3',
                'nama' => 'BATU BELAMAN',
            ],
            [
                'id' => '45',
                'district_id' => '3',
                'nama' => 'SUNGAI TENDANG',
            ],
            [
                'id' => '46',
                'district_id' => '3',
                'nama' => 'CANDI',
            ],
            [
                'id' => '47',
                'district_id' => '3',
                'nama' => 'KUMAI HULU',
            ],
            [
                'id' => '48',
                'district_id' => '3',
                'nama' => 'SUNGAI BEDAUN',
            ],
            [
                'id' => '49',
                'district_id' => '3',
                'nama' => 'SABUAI TIMUR',
            ],
            [
                'id' => '50',
                'district_id' => '3',
                'nama' => 'BUMI HARJO',
            ],
            [
                'id' => '51',
                'district_id' => '3',
                'nama' => 'PANGKALAN SATU',
            ],
            #KUMAI#

            #PANGKALAN BANTENG#
            [
                'id' => '52',
                'district_id' => '4',
                'nama' => 'PANGKALAN BANTENG',
            ],
            [
                'id' => '53',
                'district_id' => '4',
                'nama' => 'MULYA JADI',
            ],
            [
                'id' => '54',
                'district_id' => '4',
                'nama' => 'AMIN JAYA',
            ],
            [
                'id' => '55',
                'district_id' => '4',
                'nama' => 'NATAI KERBAU',
            ],
            [
                'id' => '56',
                'district_id' => '4',
                'nama' => 'KARANGMULYA',
            ],
            [
                'id' => '57',
                'district_id' => '4',
                'nama' => 'MARGA MULYA',
            ],
            [
                'id' => '58',
                'district_id' => '4',
                'nama' => 'ARGA MULYA',
            ],
            [
                'id' => '59',
                'district_id' => '4',
                'nama' => 'KEBUN AGUNG',
            ],
            [
                'id' => '60',
                'district_id' => '4',
                'nama' => 'SIDO MULYO',
            ],
            [
                'id' => '61',
                'district_id' => '4',
                'nama' => 'SIMPANG BERAMBAI',
            ],
            [
                'id' => '62',
                'district_id' => '4',
                'nama' => 'SUNGAI HIJAU',
            ],
            [
                'id' => '63',
                'district_id' => '4',
                'nama' => 'SUNGAI BENGKUANG',
            ],
            [
                'id' => '64',
                'district_id' => '4',
                'nama' => 'SUNGAI KUNING',
            ],
            [
                'id' => '65',
                'district_id' => '4',
                'nama' => 'SUNGAI PAKIT',
            ],
            [
                'id' => '66',
                'district_id' => '4',
                'nama' => 'BERAMBAI MAKMUR',
            ],
            [
                'id' => '67',
                'district_id' => '4',
                'nama' => 'KARANG SARI',
            ],
            [
                'id' => '68',
                'district_id' => '4',
                'nama' => 'SUNGAI PULAU',
            ],
            #PANGKALAN BANTENG#

            #PANGKALAN LADA#
            [
                'id' => '69',
                'district_id' => '5',
                'nama' => 'PURBASARI',
            ],
            [
                'id' => '70',
                'district_id' => '5',
                'nama' => 'SUNGAI RANGIT JAYA',
            ],
            [
                'id' => '71',
                'district_id' => '5',
                'nama' => 'SUMBER AGUNG',
            ],
            [
                'id' => '72',
                'district_id' => '5',
                'nama' => 'LADA MANDALA JAYA',
            ],
            [
                'id' => '73',
                'district_id' => '5',
                'nama' => 'MAKARTI JAYA',
            ],
            [
                'id' => '74',
                'district_id' => '5',
                'nama' => 'PANDU SENJAYA',
            ],
            [
                'id' => '75',
                'district_id' => '5',
                'nama' => 'PANGKALAN TIGA',
            ],
            [
                'id' => '76',
                'district_id' => '5',
                'nama' => 'KADIPI ATAS',
            ],
            [
                'id' => '77',
                'district_id' => '5',
                'nama' => 'PANGKALAN DEWA',
            ],
            [
                'id' => '78',
                'district_id' => '5',
                'nama' => 'PANGKALAN DURIN',
            ],
            [
                'id' => '79',
                'district_id' => '5',
                'nama' => 'SUNGAI MELAWEN',
            ],
            #PANGKALAN LADA#

            #ARUT UTARA#
            [
                'id' => '80',
                'district_id' => '6',
                'nama' => 'NANGA MUA',
            ],
            [
                'id' => '81',
                'district_id' => '6',
                'nama' => 'PANGKUT',
            ],
            [
                'id' => '82',
                'district_id' => '6',
                'nama' => 'SUKARAMI',
            ],
            [
                'id' => '83',
                'district_id' => '6',
                'nama' => 'GANDIS',
            ],
            [
                'id' => '84',
                'district_id' => '6',
                'nama' => 'KERABU',
            ],
            [
                'id' => '85',
                'district_id' => '6',
                'nama' => 'SAMBI',
            ],
            [
                'id' => '86',
                'district_id' => '6',
                'nama' => 'PENYOMBAAN',
            ],
            [
                'id' => '87',
                'district_id' => '6',
                'nama' => 'PANDAU',
            ],
            [
                'id' => '88',
                'district_id' => '6',
                'nama' => 'RIAM',
            ],
            [
                'id' => '89',
                'district_id' => '6',
                'nama' => 'PENAHAN',
            ],
            [
                'id' => '90',
                'district_id' => '6',
                'nama' => 'SUNGAI DAU',
            ],
            #ARUT UTARA#
        ]);
    }
}
