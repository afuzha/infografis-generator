<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class District extends Seeder
{
    public function run()
    {
        DB::table('district')->insert([
            [
                'id' => '1',
                'nama' => 'Kotawaringin Lama'
            ],
            [
                'id' => '2',
                'nama' => 'Arut Selatan'
            ],
            [
                'id' => '3',
                'nama' => 'Kumai'
            ],
            [
                'id' => '4',
                'nama' => 'Pangkalan Banteng'
            ],
            [
                'id' => '5',
                'nama' => 'Pangkalan Lada'
            ],
            [
                'id' => '6',
                'nama' => 'Arut Utara'
            ]
        ]);
    }
}
