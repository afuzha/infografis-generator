<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BanjirBulan extends Migration
{
    public function up()
    {
        Schema::create('banjir_bulan', function (Blueprint $table) {
            $table->id();
            $table->string('bencana_id');
            $table->string('kelurahan_terdampak')->nullable();
            $table->string('penduduk_terdampak')->nullable();
            $table->string('rumah_terendam')->nullable();
            $table->string('bulan');
            $table->string('district_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bajir_bulan');
    }
}
