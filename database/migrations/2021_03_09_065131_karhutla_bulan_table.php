<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KarhutlaBulanTable extends Migration
{
    public function up()
    {
        Schema::create('karhutla_bulan', function (Blueprint $table) {
            $table->id();
            $table->string('bencana_id');
            $table->string('jumlah_hotspot')->nullable();
            $table->string('jumlah_kejadian')->nullable();
            $table->string('luas_terbakar')->nullable();
            $table->string('bulan');
            $table->string('district_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('karhutla_bulan');
    }
}
