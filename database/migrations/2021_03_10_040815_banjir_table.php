<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BanjirTable extends Migration
{
    public function up()
    {
        Schema::create('banjir', function (Blueprint $table) {
            $table->id();
            $table->string('bencana_id');
            $table->string('bulan');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('banjir');
    }
}
