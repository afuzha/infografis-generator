    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="{{route('dashboard')}}" class="brand-link">
        <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Infografis Bencana</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- <li class="nav-item">
              <a href="{{route('dashboard')}}" class="nav-link {{ request()->is('/') ? 'active' : '' }}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li> -->
            <li class="nav-item">
              <a href="{{route('bencana.index')}}" class="nav-link {{ request()->is('bencana*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-fire"></i>
                <p>
                  Infografis Bencana
                </p>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="" class="nav-link {{ request()->is('infografik/karhutla*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-fire"></i>
                <p>
                  Infografik Karhutla
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="" class="nav-link {{ request()->is('infografik/banjir*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-water"></i>
                <p>
                  Infografik Banjir
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="" class="nav-link {{ request()->is('pengaturan*') ? 'active' : '' }}">
                <i class="nav-icon fas fa-cog"></i>
                <p>
                  Pengaturan
                </p>
              </a>
            </li> -->
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>