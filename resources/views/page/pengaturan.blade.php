@extends('master')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Starter Page</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Starter Page</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="m-0">List Nama Kecamatan <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#add-kecamatan"><i class="fa fa-plus"></i></button> </h5>
                    </div>
                    <div class="card-body">
                        @if ($kecamatan->isEmpty())
                        <p>Belum ada Nama Kecamatan yang Terdaftar...</p>
                        @else
                        <table class="table table-bordered">
                            <tr>
                                <th width="10%">No</th>
                                <th width="70%">Nama Kecamatan</th>
                                <th width="20%"></th>
                            </tr>
                            @foreach ($kecamatan as $kec)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$kec->nama}}</td>
                                <td>
                                    <a style="float:left" class="btn btn-sm btn-info" data-toggle="modal" data-target="#edit-kecamatan-{{$kec->id}}"><i class="fa fa-xs fa-edit"></i></a>
                                    <a style="float:left" class="btn btn-sm btn-danger" href="{{route('kecamatan.delete',$kec->id)}}"><i class="fa fa-xs fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="m-0">List Informasi dari Tiap Kecamatan <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#add-informasi"><i class="fa fa-plus"></i></button> </h5>
                    </div>
                    <div class="card-body">
                        @if ($informasi->isEmpty())
                        <p>Belum ada Informasi yang Terdaftar...</p>
                        @else
                        <table class="table table-bordered">
                            <tr>
                                <th width="10%">No</th>
                                <th width="70%">Informasi dari tiap Kecamatan</th>
                                <th width="20%"></th>
                            </tr>
                            @foreach ($informasi as $info)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$info->nama}}</td>
                                <td>
                                    <a style="float:left" class="btn btn-sm btn-info" data-toggle="modal" data-target="#edit-informasi-{{$info->id}}"><i class="fa fa-xs fa-edit"></i></a>
                                    <a style="float:left" class="btn btn-sm btn-danger" href="{{route('informasi.delete',$info->id)}}"><i class="fa fa-xs fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="m-0">Informasi Tambahan <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#add-options"><i class="fa fa-plus"></i></button> </h5>
                    </div>
                    <div class="card-body">
                        @if ($options->isEmpty())
                        <p>Belum ada Informasi Tambahan yang Terdaftar...</p>
                        @else
                        <table class="table table-bordered">
                            <tr>
                                <th width="10%">No</th>
                                <th width="70%">Informasi Tambahan</th>
                                <th width="20%"></th>
                            </tr>
                            @foreach ($options as $opt)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$opt->nama}}</td>
                                <td>
                                    <a style="float:left" class="btn btn-sm btn-info" data-toggle="modal" data-target="#edit-options-{{$opt->id}}"><i class="fa fa-xs fa-edit"></i></a>
                                    <a style="float:left" class="btn btn-sm btn-danger" href="{{route('options.delete',$opt->id)}}"><i class="fa fa-xs fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</div>
@endsection

@section('modal')
<div class="modal fade" id="add-kecamatan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Kecamatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('kecamatan.add') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <label for="">Nama Kecamatan</label>
                    <input id="nama" name="nama" type="text">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add-informasi">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Informasi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('informasi.add') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <label for="">Nama Informasi</label>
                    <input id="nama" name="nama" type="text">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="add-options">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Informasi Tambahan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('options.add') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <label for="">Nama Informasi Tambahan</label>
                    <input id="nama" name="nama" type="text">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@foreach ($kecamatan as $kec)
<div class="modal fade" id="edit-kecamatan-{{$kec->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Nama Kecamatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('kecamatan.update',$kec->id)}}" method="PUT">
                @csrf
                <div class="modal-body">
                    <label for="nama">Nama Kecamatan</label>
                    <input id="nama" name="nama" type="text" value="{{$kec->nama}}">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@foreach ($informasi as $info)
<div class="modal fade" id="edit-informasi-{{$info->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Informasi Tambahan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('informasi.update',$info->id)}}" method="PUT">
                @csrf
                <div class="modal-body">
                    <label for="nama">Nama Informasi Tambahan</label>
                    <input id="nama" name="nama" type="text" value="{{$info->nama}}">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@foreach ($options as $opt)
<div class="modal fade" id="edit-options-{{$opt->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Informasi Tambahan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('options.update',$opt->id)}}" method="PUT">
                @csrf
                <div class="modal-body">
                    <label for="nama">Nama Informasi Tambahan</label>
                    <input id="nama" name="nama" type="text" value="{{$opt->nama}}">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@endsection

@section('script')
<!-- <script>
    $('#edit-options').on('show.bs.modal', function(event) {

        var button = $(event.relatedTarget)
        var nama = button.data('nama')

        var modal = $(this)

        modal.find('.modal-body #nama').val(nama);
    });
</script> -->
@endsection