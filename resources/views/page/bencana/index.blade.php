@php
use Carbon\Carbon;
@endphp
@extends('master')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Data Bencana</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <!-- <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li> -->
          <li class="breadcrumb-item active">Data Bencana</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modal_tambah"><i class="fa fa-plus"></i> Tambah Data Bencana</button>
          </div>
          <div class="card-body">
            <table id="table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="4%">#</th>
                  <th>Jenis Bencana</th>
                  <th>Tahun</th>
                  <th width="14%">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($bencana as $b)
                <tr>
                  <td class="text-center">{{$loop->iteration}}</td>
                  <td>{{$b->jenis}}</td>
                  <td>{{$b->tahun}}</td>
                  <td>
                    @if($b->jenis == 'Karhutla')
                    <a href="{{route('karhutla.edit',$b->id)}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-cog"></i></a>
                    @elseif($b->jenis == 'Banjir')
                    <a href="{{route('banjir.edit',$b->id)}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-cog"></i></a>
                    @else
                    <a href="{{route('pohon.edit',$b->id)}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-cog"></i></a>
                    @endif
                    <button class="btn btn-sm btn-outline-success edit_modal" data-id="{{$b->id}}"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-sm btn-outline-danger hapus" data-id="{{$b->id}}"><i class=" fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</section>
@endsection

@section('modal')
<!-- Modal Tambah -->
<div class="modal fade" id="modal_tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Data Bencana</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('bencana.store')}}" method="post">
          @csrf
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-4 col-form-label">Jenis Bencana</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <select name="jenis" class="form-control bencana" style="width: 100%;" autocomplete="off">
                    <option></option>
                    @foreach($jenis as $j)
                    <option value="{{$j}}">{{$j}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-4 col-form-label">Tahun</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <select name="tahun" class="form-control tahun" style="width: 100%;" autocomplete="off">
                    <option></option>
                    @for ($i = 2021; $i >= 2015; $i--)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</div>
<!-- Modal Tambah -->

<!-- Modal Edit -->
<div class="modal fade" id="modal_edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Data Bencana</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" method="POST">
          @csrf
          @method('put')
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-4 col-form-label">Jenis Bencana</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <select name="jenis" id="jenis" class="form-control bencana" style="width: 100%;" autocomplete="off">
                    <option></option>
                    @foreach($jenis as $j)
                    <option value="{{$j}}">{{$j}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-4 col-form-label">Tahun</label>
              <div class="col-sm-8">
                <div class="form-group">
                  <select name="tahun" id="tahun" class="form-control tahun" style="width: 100%;" autocomplete="off">
                    <option></option>
                    @for ($i = 2021; $i >= 2015; $i--)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Ubah</button>
            </div>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</div>
<!-- Modal Edit -->
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script>
  $(function() {
    $("#table").DataTable({
      "responsive": true,
      "autoWidth": false,
    });

    $('.bencana').select2({
      minimumResultsForSearch: Infinity,
      placeholder: '--Pilih Jenis Bencana--'
    })
    $('.tahun').select2({
      minimumResultsForSearch: Infinity,
      placeholder: '--Pilih Tahun--'
    })
    $('#bulan').datepicker({
      format: "mm",
      startView: 1,
      minViewMode: 1,
      maxViewMode: 1,
      language: "id",
      autoclose: true
    })
    $('#tahun').datepicker({
      format: "yyyy",
      startView: 2,
      minViewMode: 2,
      maxViewMode: 2,
      language: "id",
      autoclose: true
    })
  });
</script>
<script>
  $('#table').on('click', '.edit_modal', function() {
    var $id = $(this).attr('data-id');
    $(".modal-body form").attr("action", "bencana" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.bencana_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id
      },
      dataType: "JSON",
      success: function(data) {
        $('#modal_edit').modal('show');
        $("#jenis").val(data.jenis).change();
        $("#tahun").val(data.tahun).change();
        console.log(data);
      }
    });
  });
</script>
<script>
  //script konfirmasi hapus
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  $('.hapus').click(function(e) {
    // id = e.target.dataset.id;
    e.preventDefault();
    var delete_id = $(this).attr('data-id');
    Swal.fire({
      title: 'Apakah anda yakin?',
      text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Sekarang!',
      cancelButtonText: 'Batalkan'
    }).then(function(result) {
      if (result.value) {
        var data = {
          "_token": $('input[name=_token]').val(),
          "id": delete_id
        }
        $.ajax({
          type: "DELETE",
          url: '/bencana/' + delete_id,
          data: data,
          success: function(data) {
            Swal.fire(
                'Berhasil!',
                'Data Bencana berhasil dihapus...',
                'success'
              )
              .then((result) => {
                location.reload();
              })
            // console.log(result)
          }
        })
      } else {
        // console.log(`dialog was dismissed by ${result.dismiss}`)
        Swal.fire(
          'Gagal!',
          'Hapus data telah dibatalkan...',
          'error'
        )
      }
    })
  })
</script>

@if(session("success"))
<script>
  const Toast = Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  Toast.fire({
    icon: 'success',
    title: '{{session("isi")}}'
  })
</script>
@endif
@endsection