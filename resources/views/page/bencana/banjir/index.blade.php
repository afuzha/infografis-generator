@extends('master')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Data Bencana {{$bencana->jenis}} Tahun {{$bencana->tahun}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <!-- <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li> -->
          <li class="breadcrumb-item"><a href="{{route('bencana.index')}}">Data Bencana</a></li>
          <li class="breadcrumb-item active">{{$bencana->jenis}} {{$bencana->tahun}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <a href="{{route('bencana.index')}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
            @if($data_bulan -> isEmpty())
            @else
            <a href="{{route('download.banjir.tahun.image',$bencana->id)}}" class="btn btn-sm btn-primary float-sm-right"><i class="fa fa-download"></i> Download Infografis Tahun {{$bencana->tahun}}</a>
            @endif
          </div>
          <div class="card-header">
            @if($banjir_bulan->isEmpty())
            <form action="{{route('banjir.store')}}" method="post">
              @csrf
              @foreach($bulan as $b)
              <input name="bencana_id[]" value="{{$bencana->id}}" type="hidden">
              <input name="bulan[]" value="{{$b}}" type="hidden">
              @endforeach
              <button type="submit" class=" btn btn-sm btn-outline-primary">Buat Bulan</button>
            </form>
            @else

            @if($januari->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Januari'])}}" class="btn btn-sm btn-outline-primary">Januari</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Januari'])}}" class="btn btn-sm btn-outline-success">Januari</a>
            @endif
            @if($februari->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Februari'])}}" class="btn btn-sm btn-outline-primary">Februari</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Februari'])}}" class="btn btn-sm btn-outline-success">Februari</a>
            @endif
            @if($maret->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Maret'])}}" class="btn btn-sm btn-outline-primary">Maret</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Maret'])}}" class="btn btn-sm btn-outline-success">Maret</a>
            @endif
            @if($april->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'April'])}}" class="btn btn-sm btn-outline-primary">April</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'April'])}}" class="btn btn-sm btn-outline-success">April</a>
            @endif
            @if($mei->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Mei'])}}" class="btn btn-sm btn-outline-primary">Mei</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Mei'])}}" class="btn btn-sm btn-outline-success">Mei</a>
            @endif
            @if($juni->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Juni'])}}" class="btn btn-sm btn-outline-primary">Juni</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Juni'])}}" class="btn btn-sm btn-outline-success">Juni</a>
            @endif
            @if($juli->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Juli'])}}" class="btn btn-sm btn-outline-primary">Juli</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Juli'])}}" class="btn btn-sm btn-outline-success">Juli</a>
            @endif
            @if($agustus->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Agustus'])}}" class="btn btn-sm btn-outline-primary">Agustus</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Agustus'])}}" class="btn btn-sm btn-outline-success">Agustus</a>
            @endif
            @if($september->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'September'])}}" class="btn btn-sm btn-outline-primary">September</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'September'])}}" class="btn btn-sm btn-outline-success">September</a>
            @endif
            @if($oktober->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Oktober'])}}" class="btn btn-sm btn-outline-primary">Oktober</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Oktober'])}}" class="btn btn-sm btn-outline-success">Oktober</a>
            @endif
            @if($november->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'November'])}}" class="btn btn-sm btn-outline-primary">November</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'November'])}}" class="btn btn-sm btn-outline-success">November</a>
            @endif
            @if($desember->isEmpty())
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Desember'])}}" class="btn btn-sm btn-outline-primary">Desember</a>
            @else
            <a href="{{route('banjir.bulan.edit',[$bencana->id,'Desember'])}}" class="btn btn-sm btn-outline-success">Desember</a>
            @endif

            @endif
          </div>

          <div class="card-body">
            <table class="table table-bordered" id="tablze">
              <thead>
                <tr class="text-center">
                  <th>Kecamatan</th>
                  <th>Desa/Kelurahan Terdampak</th>
                  <th>Penduduk Terdampak</th>
                  <th>Rumah Terendam</th>
                </tr>
                <tr class="text-center">
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Kotawaringin Lama</td>
                  <td class="text-center">{{$kelurahan1}}</td>
                  <td class="text-center">{{$penduduk1}}</td>
                  <td class="text-center">{{$rumah1}}</td>
                </tr>
                <tr>
                  <td>Arut Selatan</td>
                  <td class="text-center">{{$kelurahan2}}</td>
                  <td class="text-center">{{$penduduk2}}</td>
                  <td class="text-center">{{$rumah2}}</td>
                </tr>
                <tr>
                  <td>Kumai</td>
                  <td class="text-center">{{$kelurahan3}}</td>
                  <td class="text-center">{{$penduduk3}}</td>
                  <td class="text-center">{{$rumah3}}</td>
                </tr>
                <tr>
                  <td>Pangkalan Banteng</td>
                  <td class="text-center">{{$kelurahan4}}</td>
                  <td class="text-center">{{$penduduk4}}</td>
                  <td class="text-center">{{$rumah4}}</td>
                </tr>
                <tr>
                  <td>Pangkalan Lada</td>
                  <td class="text-center">{{$kelurahan5}}</td>
                  <td class="text-center">{{$penduduk5}}</td>
                  <td class="text-center">{{$rumah5}}</td>
                </tr>
                <tr>
                  <td>Arut Utara</td>
                  <td class="text-center">{{$kelurahan6}}</td>
                  <td class="text-center">{{$penduduk6}}</td>
                  <td class="text-center">{{$rumah6}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</section>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>
@if(session("success"))
<script>
  const Toast = Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  Toast.fire({
    icon: 'success',
    title: '{{session("isi")}}'
  })
</script>
@endif

@endsection