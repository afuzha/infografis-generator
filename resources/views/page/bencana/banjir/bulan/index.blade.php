@php
use Carbon\Carbon;
@endphp
@extends('master')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">
@endsection

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Data Bencana {{$bencana->jenis}} {{$getbulan->bulan}} {{$bencana->tahun}}</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <!-- <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li> -->
          <li class="breadcrumb-item"><a href="{{route('bencana.index')}}">Data Bencana</a></li>
          <li class="breadcrumb-item active">{{$bencana->jenis}} {{$getbulan->bulan}} {{$bencana->tahun}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <a href="{{route('banjir.edit',$bencana->id)}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
            @if($ifkolam -> isEmpty() || $ifarsel -> isEmpty() || $ifkumai -> isEmpty() || $ifbanteng -> isEmpty() || $iflada -> isEmpty() || $ifaruta -> isEmpty())
            @else
            <a href="{{route('download.banjir.bulan.image',[$bencana->id,$getbulan->bulan])}}" class="btn btn-sm btn-primary float-sm-right"><i class="fa fa-download"></i> Download Infografis {{$getbulan->bulan}} {{$bencana->tahun}}</a>
            @endif
          </div>
          <div class="card-header">
            @if($ifkolam -> isEmpty())
            <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#kotawaringin_lama"><i class="fa fa-plus"></i> Kotawaringin Lama</button>
            @else
            @foreach($ifkolam as $i)
            <button class="btn btn-sm btn-outline-success kolam" data-bulan="{{$i->bulan}}" data-id="{{$i->id}}" data-bencana_id="{{$bencana->id}}" data-district_id="1"><i class="fa fa-edit"></i> Kotawaringin Lama</button>
            @endforeach
            @endif
            @if($ifarsel -> isEmpty())
            <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#arut_selatan"><i class="fa fa-plus"></i> Arut Selatan</button>
            @else
            @foreach($ifarsel as $i)
            <button class="btn btn-sm btn-outline-success arsel" data-bulan="{{$i->bulan}}" data-id="{{$i->id}}" data-bencana_id="{{$bencana->id}}" data-district_id="2"><i class="fa fa-edit"></i> Arut Selatan</button>
            @endforeach
            @endif
            @if($ifkumai -> isEmpty())
            <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#kumai"><i class="fa fa-plus"></i> Kumai</button>
            @else
            @foreach($ifkumai as $i)
            <button class="btn btn-sm btn-outline-success kumai" data-bulan="{{$i->bulan}}" data-id="{{$i->id}}" data-bencana_id="{{$bencana->id}}" data-district_id="3"><i class="fa fa-edit"></i> Kumai</button>
            @endforeach
            @endif
            @if($ifbanteng -> isEmpty())
            <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#pangkalan_banteng"><i class="fa fa-plus"></i> Pangkalan Banteng</button>
            @else
            @foreach($ifbanteng as $i)
            <button class="btn btn-sm btn-outline-success banteng" data-bulan="{{$i->bulan}}" data-id="{{$i->id}}" data-bencana_id="{{$bencana->id}}" data-district_id="4"><i class="fa fa-edit"></i> Pangkalan Banteng</button>
            @endforeach
            @endif
            @if($iflada -> isEmpty())
            <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#pangkalan_lada"><i class="fa fa-plus"></i> Pangkalan Lada</button>
            @else
            @foreach($iflada as $i)
            <button class="btn btn-sm btn-outline-success lada" data-bulan="{{$i->bulan}}" data-id="{{$i->id}}" data-bencana_id="{{$bencana->id}}" data-district_id="5"><i class="fa fa-edit"></i> Pangkalan Lada</button>
            @endforeach
            @endif
            @if($ifaruta -> isEmpty())
            <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#arut_utara"><i class="fa fa-plus"></i> Arut Utara</button>
            @else
            @foreach($ifaruta as $i)
            <button class="btn btn-sm btn-outline-success aruta" data-bulan="{{$i->bulan}}" data-id="{{$i->id}}" data-bencana_id="{{$bencana->id}}" data-district_id="6"><i class="fa fa-edit"></i> Arut Utara</button>
            @endforeach
            @endif
          </div>
          <div class="card-body">
            <table class="table table-bordered" id="tablze">
              <thead>
                <tr class="text-center">
                  <th>Kecamatan</th>
                  <th>Desa/Kelurahan Terdampak</th>
                  <th>Penduduk Terdampak</th>
                  <th>Rumah Terendam</th>
                </tr>
                <tr class="text-center">
                </tr>
              </thead>
              <tbody>
                @foreach($databulan as $k)
                <tr>
                  <td>{{$k->district->nama}}</td>
                  <td class="text-center">{{$k->kelurahan_terdampak}}</td>
                  <td class="text-center">{{$k->penduduk_terdampak}}</td>
                  <td class="text-center">{{$k->rumah_terendam}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</section>
@endsection

@section('modal')
@include('page.bencana.banjir.bulan.modal_edit')
@endsection



@section('script')
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>

@include('page.bencana.banjir.bulan.js')
<script>
  //script konfirmasi hapus
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  $('.hapus').click(function(e) {
    // id = e.target.dataset.id;
    e.preventDefault();
    var delete_id = $(this).attr('data-id');
    Swal.fire({
      title: 'Apakah anda yakin?',
      text: "Anda tidak dapat mengembalikan data yang telah dihapus!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Sekarang!',
      cancelButtonText: 'Batalkan'
    }).then(function(result) {
      if (result.value) {
        var data = {
          "_token": $('input[name=_token]').val(),
          "id": delete_id
        }
        $.ajax({
          type: "DELETE",
          url: '/bencana/' + delete_id,
          data: data,
          success: function(data) {
            Swal.fire(
                'Berhasil!',
                'Data Bencana berhasil dihapus...',
                'success'
              )
              .then((result) => {
                location.reload();
              })
            // console.log(result)
          }
        })
      } else {
        // console.log(`dialog was dismissed by ${result.dismiss}`)
        Swal.fire(
          'Gagal!',
          'Hapus data telah dibatalkan...',
          'error'
        )
      }
    })
  })
</script>

@if(session("success"))
<script>
  const Toast = Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  Toast.fire({
    icon: 'success',
    title: '{{session("isi")}}'
  })
</script>
@endif

<script>
  $(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#kecamatan').on('change', function() {
      $.ajax({
        url: '{{ route("getkelurahan") }}',
        method: 'POST',
        data: {
          id: $(this).val()
        },
        success: function(response) {
          $('#kelurahan').empty();
          $.each(response, function(id, nama) {
            $('#kelurahan').append(new Option(nama, id))
          })
          console.log(response)
        }
      })
    });
  });
</script>
@include('page.bencana.banjir.bulan.validation_js')
@endsection