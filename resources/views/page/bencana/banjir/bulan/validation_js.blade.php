<script type="text/javascript">
  $(document).ready(function() {
    $('#kolam').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#kolam_edit').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#arsel').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#arsel_edit').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#kumai').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#kumai_edit').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#banteng').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#banteng_edit').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#lada').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#lada_edit').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#aruta').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#aruta_edit').validate({
      rules: {
        kelurahan_terdampak: {
          required: true
        },
        penduduk_terdampak: {
          required: true
        },
        rumah_terendam: {
          required: true
        },
      },
      messages: {
        kelurahan_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        penduduk_terdampak: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        rumah_terendam: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>