<!-- Kolam -->
<script>
  $('.card-header').on('click', '.kolam', function() {
    var $id = $(this).attr('data-id');
    var $bulan = $(this).attr('data-bulan')
    var $district_id = $(this).attr('data-district_id');
    var $bencana_id = $(this).attr('data-bencana_id');
    // alert($id);
    $(".modal-body form").attr("action", "/bencana/banjir/data/b" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.banjirbulan_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id,
        bulan: $bulan,
        district_id: $district_id,
        bencana_id: $bencana_id,
      },
      dataType: "JSON",
      success: function(data) {
        $('#modal_kotawaringin_lama').modal('show');
        $("#kolam_kelurahan").val(data.kelurahan_terdampak);
        $("#kolam_penduduk").val(data.penduduk_terdampak);
        $("#kolam_rumah").val(data.rumah_terendam);
        $("#kolam_bencana_id").val(data.bencana_id);
        $("#kolam_bulan").val(data.bulan);
        $("#kolam_district_id").val(data.district_id);
        console.log(data);
      }
    });
  });
</script>
<!-- Kolam -->

<!-- Arsel -->
<script>
  $('.card-header').on('click', '.arsel', function() {
    var $id = $(this).attr('data-id');
    var $bulan = $(this).attr('data-bulan')
    var $district_id = $(this).attr('data-district_id');
    var $bencana_id = $(this).attr('data-bencana_id');
    // alert($id);
    $(".modal-body form").attr("action", "/bencana/banjir/data/b" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.banjirbulan_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id,
        bulan: $bulan,
        district_id: $district_id,
        bencana_id: $bencana_id,
      },
      dataType: "JSON",
      success: function(data) {
        $('#modal_arut_selatan').modal('show');
        $("#arsel_kelurahan").val(data.kelurahan_terdampak);
        $("#arsel_penduduk").val(data.penduduk_terdampak);
        $("#arsel_rumah").val(data.rumah_terendam);
        $("#arsel_bencana_id").val(data.bencana_id);
        $("#arsel_bulan").val(data.bulan);
        $("#arsel_district_id").val(data.district_id);
        console.log(data);
      }
    });
  });
</script>
<!-- Arsel -->

<!-- Kumai -->
<script>
  $('.card-header').on('click', '.kumai', function() {
    var $id = $(this).attr('data-id');
    var $bulan = $(this).attr('data-bulan')
    var $district_id = $(this).attr('data-district_id');
    var $bencana_id = $(this).attr('data-bencana_id');
    // alert($id);
    $(".modal-body form").attr("action", "/bencana/banjir/data/b" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.banjirbulan_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id,
        bulan: $bulan,
        district_id: $district_id,
        bencana_id: $bencana_id,
      },
      dataType: "JSON",
      success: function(data) {
        $('#modal_kumai').modal('show');
        $("#kumai_kelurahan").val(data.kelurahan_terdampak);
        $("#kumai_penduduk").val(data.penduduk_terdampak);
        $("#kumai_rumah").val(data.rumah_terendam);
        $("#kumai_bencana_id").val(data.bencana_id);
        $("#kumai_bulan").val(data.bulan);
        $("#kumai_district_id").val(data.district_id);
        console.log(data);
      }
    });
  });
</script>
<!-- Kumai -->

<!-- Banteng -->
<script>
  $('.card-header').on('click', '.banteng', function() {
    var $id = $(this).attr('data-id');
    var $bulan = $(this).attr('data-bulan')
    var $district_id = $(this).attr('data-district_id');
    var $bencana_id = $(this).attr('data-bencana_id');
    // alert($id);
    $(".modal-body form").attr("action", "/bencana/banjir/data/b" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.banjirbulan_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id,
        bulan: $bulan,
        district_id: $district_id,
        bencana_id: $bencana_id,
      },
      dataType: "JSON",
      success: function(data) {
        $('#modal_banteng').modal('show');
        $("#banteng_kelurahan").val(data.kelurahan_terdampak);
        $("#banteng_penduduk").val(data.penduduk_terdampak);
        $("#banteng_rumah").val(data.rumah_terendam);
        $("#banteng_bencana_id").val(data.bencana_id);
        $("#banteng_bulan").val(data.bulan);
        $("#banteng_district_id").val(data.district_id);
        console.log(data);
      }
    });
  });
</script>
<!-- Banteng -->

<!-- Lada -->
<script>
  $('.card-header').on('click', '.lada', function() {
    var $id = $(this).attr('data-id');
    var $bulan = $(this).attr('data-bulan')
    var $district_id = $(this).attr('data-district_id');
    var $bencana_id = $(this).attr('data-bencana_id');
    // alert($id);
    $(".modal-body form").attr("action", "/bencana/banjir/data/b" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.banjirbulan_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id,
        bulan: $bulan,
        district_id: $district_id,
        bencana_id: $bencana_id,
      },
      dataType: "JSON",
      success: function(data) {
        $('#modal_lada').modal('show');
        $("#lada_kelurahan").val(data.kelurahan_terdampak);
        $("#lada_penduduk").val(data.penduduk_terdampak);
        $("#lada_rumah").val(data.rumah_terendam);
        $("#lada_bencana_id").val(data.bencana_id);
        $("#lada_bulan").val(data.bulan);
        $("#lada_district_id").val(data.district_id);
        console.log(data);
      }
    });
  });
</script>
<!-- Lada -->

<!-- Aruta -->
<script>
  $('.card-header').on('click', '.aruta', function() {
    var $id = $(this).attr('data-id');
    var $bulan = $(this).attr('data-bulan')
    var $district_id = $(this).attr('data-district_id');
    var $bencana_id = $(this).attr('data-bencana_id');
    // alert($id);
    $(".modal-body form").attr("action", "/bencana/banjir/data/b" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.banjirbulan_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id,
        bulan: $bulan,
        district_id: $district_id,
        bencana_id: $bencana_id,
      },
      dataType: "JSON",
      success: function(data) {
        $('#modal_aruta').modal('show');
        $("#aruta_kelurahan").val(data.kelurahan_terdampak);
        $("#aruta_penduduk").val(data.penduduk_terdampak);
        $("#aruta_rumah").val(data.rumah_terendam);
        $("#aruta_bencana_id").val(data.bencana_id);
        $("#aruta_bulan").val(data.bulan);
        $("#aruta_district_id").val(data.district_id);
        console.log(data);
      }
    });
  });
</script>
<!-- Aruta -->