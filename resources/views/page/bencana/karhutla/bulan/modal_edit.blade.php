<!-- Modal Tambah Kotawaringin Lama -->
<div class="modal fade" id="kotawaringin_lama">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Kotawaringin Lama</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('k.store')}}" method="post" id="kolam">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" id="inputEmail3" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kejadian" class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" id="kejadian" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" id="inputEmail3" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" value="{{$bencana->id}}" type="hidden">
                        <input name="district_id" value="1" type="hidden">
                        <input name="bulan" value="{{$getbulan->bulan}}" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Tambah Kotawaringin Lama -->

<!-- Modal Edit Kotawaringin Lama -->
<div class="modal fade" id="modal_kotawaringin_lama">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data {{$kolam->nama}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="#" id="kolam_edit">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="kolam_hotspot" class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" id="kolam_hotspot" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kolam_kejadian" class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" id="kolam_kejadian" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kolam_luas_terbakar" class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" id="kolam_luas_terbakar" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" id="kolam_bencana_id" value="" type="hidden">
                        <input name="district_id" id="kolam_district_id" value="" type="hidden">
                        <input name="bulan" id="kolam_bulan" value="" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Edit Kotawaringin Lama -->

<!-- Modal Tambah Arut Selatan -->
<div class="modal fade" id="arut_selatan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Arut Selatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('k.store')}}" method="post" id="arsel">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" value="{{$bencana->id}}" type="hidden">
                        <input name="district_id" value="2" type="hidden">
                        <input name="bulan" value="{{$getbulan->bulan}}" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Tambah Arut Selatan -->

<!-- Modal Edit Arut Selatan -->
<div class="modal fade" id="modal_arut_selatan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data {{$arsel->nama}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="#" id="arsel_edit">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="arsel_hotspot" class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" id="arsel_hotspot" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="arsel_kejadian" class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" id="arsel_kejadian" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="arsel_luas_terbakar" class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" id="arsel_luas_terbakar" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" id="arsel_bencana_id" value="" type="hidden">
                        <input name="district_id" id="arsel_district_id" value="" type="hidden">
                        <input name="bulan" id="arsel_bulan" value="" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Edit Arut Selatan -->

<!-- Modal Tambah Kumai -->
<div class="modal fade" id="kumai">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Kumai</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('k.store')}}" method="post" id="kumai">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" value="{{$bencana->id}}" type="hidden">
                        <input name="district_id" value="3" type="hidden">
                        <input name="bulan" value="{{$getbulan->bulan}}" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Tambah Kumai -->

<!-- Modal Edit Kumai -->
<div class="modal fade" id="modal_kumai">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data {{$kumai->nama}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="#" id="kumai_edit">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="kumai_hotspot" class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" id="kumai_hotspot" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kumai_kejadian" class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" id="kumai_kejadian" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kumai_luas_terbakar" class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" id="kumai_luas_terbakar" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" id="kumai_bencana_id" value="" type="hidden">
                        <input name="district_id" id="kumai_district_id" value="" type="hidden">
                        <input name="bulan" id="kumai_bulan" value="" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Edit Kumai -->

<!-- Modal Tambah Pangkalan Banteng -->
<div class="modal fade" id="pangkalan_banteng">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Pangkalan Banteng</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('k.store')}}" method="post" id="banteng">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" value="{{$bencana->id}}" type="hidden">
                        <input name="district_id" value="4" type="hidden">
                        <input name="bulan" value="{{$getbulan->bulan}}" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Tambah Pangkalan Banteng -->

<!-- Modal Edit Pangkalan Banteng -->
<div class="modal fade" id="modal_banteng">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data {{$banteng->nama}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="#" id="banteng_edit">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="banteng_hotspot" class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" id="banteng_hotspot" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="banteng_kejadian" class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" id="banteng_kejadian" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="banteng_luas_terbakar" class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" id="banteng_luas_terbakar" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" id="banteng_bencana_id" value="" type="hidden">
                        <input name="district_id" id="banteng_district_id" value="" type="hidden">
                        <input name="bulan" id="banteng_bulan" value="" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Edit Pangkalan Banteng -->

<!-- Modal Tambah Lada -->
<div class="modal fade" id="pangkalan_lada">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Pangkalan Lada</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('k.store')}}" method="post" id="lada">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" value="{{$bencana->id}}" type="hidden">
                        <input name="district_id" value="5" type="hidden">
                        <input name="bulan" value="{{$getbulan->bulan}}" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Tambah Lada -->

<!-- Modal Edit Lada -->
<div class="modal fade" id="modal_lada">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data {{$lada->nama}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="#" id="lada_edit">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="lada_hotspot" class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" id="lada_hotspot" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lada_kejadian" class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" id="lada_kejadian" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lada_luas_terbakar" class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" id="lada_luas_terbakar" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" id="lada_bencana_id" value="" type="hidden">
                        <input name="district_id" id="lada_district_id" value="" type="hidden">
                        <input name="bulan" id="lada_bulan" value="" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Edit Lada -->





<!-- Modal Tambah Aruta -->
<div class="modal fade" id="arut_utara">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Arut Utara</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('k.store')}}" method="post" id="aruta">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" value="{{$bencana->id}}" type="hidden">
                        <input name="district_id" value="6" type="hidden">
                        <input name="bulan" value="{{$getbulan->bulan}}" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Tambah Aruta -->

<!-- Modal Edit Aruta -->
<div class="modal fade" id="modal_aruta">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data {{$aruta->nama}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="#" id="aruta_edit">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="aruta_hotspot" class="col-sm-4 col-form-label">Hotspot</label>
                            <div class="col-sm-8">
                                <input name="jumlah_hotspot" type="number" class="form-control" id="aruta_hotspot" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="aruta_kejadian" class="col-sm-4 col-form-label">Kejadian</label>
                            <div class="col-sm-8">
                                <input name="jumlah_kejadian" type="number" class="form-control" id="aruta_kejadian" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="aruta_luas_terbakar" class="col-sm-4 col-form-label">Luas Terbakar</label>
                            <div class="col-sm-8">
                                <input name="luas_terbakar" type="number" class="form-control" id="aruta_luas_terbakar" placeholder="Masukan jumlah...">
                            </div>
                        </div>
                        <input name="bencana_id" id="aruta_bencana_id" value="" type="hidden">
                        <input name="district_id" id="aruta_district_id" value="" type="hidden">
                        <input name="bulan" id="aruta_bulan" value="" type="hidden">
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- Modal Edit Aruta -->