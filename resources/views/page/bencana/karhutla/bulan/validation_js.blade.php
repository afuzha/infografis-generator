<script type="text/javascript">
  $(document).ready(function() {
    $('#kolam').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#kolam_edit').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#arsel').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#arsel_edit').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#kumai').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#kumai_edit').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#banteng').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#banteng_edit').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#lada').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#lada_edit').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#aruta').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
    $('#aruta_edit').validate({
      rules: {
        jumlah_hotspot: {
          required: true
        },
        jumlah_kejadian: {
          required: true
        },
        luas_terbakar: {
          required: true
        },
      },
      messages: {
        jumlah_hotspot: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        jumlah_kejadian: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
        luas_terbakar: {
          required: "Isi 0 apabila tidak mempunyai data...",
        },
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.col-sm-8').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>