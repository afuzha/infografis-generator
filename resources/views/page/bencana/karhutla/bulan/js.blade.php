<!-- Kolam -->
<script>
    $('.card-header').on('click', '.kolam', function() {
        var $id = $(this).attr('data-id');
        var $bulan = $(this).attr('data-bulan')
        var $district_id = $(this).attr('data-district_id');
        var $bencana_id = $(this).attr('data-bencana_id');
        // alert($id);
        $(".modal-body form").attr("action", "/bencana/karhutla/data/k" + "/" + $id);
        $.ajax({
            type: "POST",
            url: "{{ route('json.karhutlabulan_edit') }}",
            data: {
                token: '{{ csrf_token() }}',
                id: $id,
                bulan: $bulan,
                district_id: $district_id,
                bencana_id: $bencana_id,
            },
            dataType: "JSON",
            success: function(data) {
                $('#modal_kotawaringin_lama').modal('show');
                $("#kolam_kejadian").val(data.jumlah_kejadian);
                $("#kolam_hotspot").val(data.jumlah_hotspot);
                $("#kolam_luas_terbakar").val(data.luas_terbakar);
                $("#kolam_bencana_id").val(data.bencana_id);
                $("#kolam_bulan").val(data.bulan);
                $("#kolam_district_id").val(data.district_id);
                console.log(data);
            }
        });
    });
</script>
<!-- Kolam -->

<!-- Arsel -->
<script>
    $('.card-header').on('click', '.arsel', function() {
        var $id = $(this).attr('data-id');
        var $bulan = $(this).attr('data-bulan')
        var $district_id = $(this).attr('data-district_id');
        var $bencana_id = $(this).attr('data-bencana_id');
        // alert($id);
        $(".modal-body form").attr("action", "/bencana/karhutla/data/k" + "/" + $id);
        $.ajax({
            type: "POST",
            url: "{{ route('json.karhutlabulan_edit') }}",
            data: {
                token: '{{ csrf_token() }}',
                id: $id,
                bulan: $bulan,
                district_id: $district_id,
                bencana_id: $bencana_id,
            },
            dataType: "JSON",
            success: function(data) {
                $('#modal_arut_selatan').modal('show');
                $("#arsel_kejadian").val(data.jumlah_kejadian);
                $("#arsel_hotspot").val(data.jumlah_hotspot);
                $("#arsel_luas_terbakar").val(data.luas_terbakar);
                $("#arsel_bencana_id").val(data.bencana_id);
                $("#arsel_bulan").val(data.bulan);
                $("#arsel_district_id").val(data.district_id);
                console.log(data);
            }
        });
    });
</script>
<!-- Arsel -->

<!-- Kumai -->
<script>
    $('.card-header').on('click', '.kumai', function() {
        var $id = $(this).attr('data-id');
        var $bulan = $(this).attr('data-bulan')
        var $district_id = $(this).attr('data-district_id');
        var $bencana_id = $(this).attr('data-bencana_id');
        // alert($id);
        $(".modal-body form").attr("action", "/bencana/karhutla/data/k" + "/" + $id);
        $.ajax({
            type: "POST",
            url: "{{ route('json.karhutlabulan_edit') }}",
            data: {
                token: '{{ csrf_token() }}',
                id: $id,
                bulan: $bulan,
                district_id: $district_id,
                bencana_id: $bencana_id,
            },
            dataType: "JSON",
            success: function(data) {
                $('#modal_kumai').modal('show');
                $("#kumai_kejadian").val(data.jumlah_kejadian);
                $("#kumai_hotspot").val(data.jumlah_hotspot);
                $("#kumai_luas_terbakar").val(data.luas_terbakar);
                $("#kumai_bencana_id").val(data.bencana_id);
                $("#kumai_bulan").val(data.bulan);
                $("#kumai_district_id").val(data.district_id);
                console.log(data);
            }
        });
    });
</script>
<!-- Kumai -->

<!-- Banteng -->
<script>
    $('.card-header').on('click', '.banteng', function() {
        var $id = $(this).attr('data-id');
        var $bulan = $(this).attr('data-bulan')
        var $district_id = $(this).attr('data-district_id');
        var $bencana_id = $(this).attr('data-bencana_id');
        // alert($id);
        $(".modal-body form").attr("action", "/bencana/karhutla/data/k" + "/" + $id);
        $.ajax({
            type: "POST",
            url: "{{ route('json.karhutlabulan_edit') }}",
            data: {
                token: '{{ csrf_token() }}',
                id: $id,
                bulan: $bulan,
                district_id: $district_id,
                bencana_id: $bencana_id,
            },
            dataType: "JSON",
            success: function(data) {
                $('#modal_banteng').modal('show');
                $("#banteng_kejadian").val(data.jumlah_kejadian);
                $("#banteng_hotspot").val(data.jumlah_hotspot);
                $("#banteng_luas_terbakar").val(data.luas_terbakar);
                $("#banteng_bencana_id").val(data.bencana_id);
                $("#banteng_bulan").val(data.bulan);
                $("#banteng_district_id").val(data.district_id);
                console.log(data);
            }
        });
    });
</script>
<!-- Banteng -->

<!-- Lada -->
<script>
    $('.card-header').on('click', '.lada', function() {
        var $id = $(this).attr('data-id');
        var $bulan = $(this).attr('data-bulan')
        var $district_id = $(this).attr('data-district_id');
        var $bencana_id = $(this).attr('data-bencana_id');
        // alert($id);
        $(".modal-body form").attr("action", "/bencana/karhutla/data/k" + "/" + $id);
        $.ajax({
            type: "POST",
            url: "{{ route('json.karhutlabulan_edit') }}",
            data: {
                token: '{{ csrf_token() }}',
                id: $id,
                bulan: $bulan,
                district_id: $district_id,
                bencana_id: $bencana_id,
            },
            dataType: "JSON",
            success: function(data) {
                $('#modal_lada').modal('show');
                $("#lada_kejadian").val(data.jumlah_kejadian);
                $("#lada_hotspot").val(data.jumlah_hotspot);
                $("#lada_luas_terbakar").val(data.luas_terbakar);
                $("#lada_bencana_id").val(data.bencana_id);
                $("#lada_bulan").val(data.bulan);
                $("#lada_district_id").val(data.district_id);
                console.log(data);
            }
        });
    });
</script>
<!-- Lada -->

<!-- Aruta -->
<script>
    $('.card-header').on('click', '.aruta', function() {
        var $id = $(this).attr('data-id');
        var $bulan = $(this).attr('data-bulan')
        var $district_id = $(this).attr('data-district_id');
        var $bencana_id = $(this).attr('data-bencana_id');
        // alert($id);
        $(".modal-body form").attr("action", "/bencana/karhutla/data/k" + "/" + $id);
        $.ajax({
            type: "POST",
            url: "{{ route('json.karhutlabulan_edit') }}",
            data: {
                token: '{{ csrf_token() }}',
                id: $id,
                bulan: $bulan,
                district_id: $district_id,
                bencana_id: $bencana_id,
            },
            dataType: "JSON",
            success: function(data) {
                $('#modal_aruta').modal('show');
                $("#aruta_kejadian").val(data.jumlah_kejadian);
                $("#aruta_hotspot").val(data.jumlah_hotspot);
                $("#aruta_luas_terbakar").val(data.luas_terbakar);
                $("#aruta_bencana_id").val(data.bencana_id);
                $("#aruta_bulan").val(data.bulan);
                $("#aruta_district_id").val(data.district_id);
                console.log(data);
            }
        });
    });
</script>
<!-- Aruta -->